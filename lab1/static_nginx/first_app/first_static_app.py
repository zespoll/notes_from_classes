from flask import Flask, request
from flask import jsonify

app = Flask(__name__)

collected_names = {}

def get_names():
    return jsonify( all_names = list(collected_names.values()) )

def save_new_name(name):
    name_id = len(collected_names) + 1
    collected_names[ name_id ] = name
    return jsonify(added_name = name)

@app.route("/namesApi/", methods=["GET", "POST"])
def names():
    if request.method == "GET":
        return get_names()
    elif request.method == "POST":
        name = request.form.get("name", "")
        return save_new_name(name)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
