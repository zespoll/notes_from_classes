# Zajęcia
1. [Lab_1](#markdown-header-lab-1-docker)
2. [Lab_2](#markdown-header-lab-2-html)
3. [Lab_3](#markdown-header-lab-3-ajax)
4. [Lab_4](#markdown-header-lab-4-server-side)
5. [Lab_5](#markdown-header-lab-5-redis)
6. [Lab_6](#markdown-header-lab-6-redis-and-session)
7. [Lab_7](#markdown-header-lab-7-jwt)
8. [Lab_8](#markdown-header-lab-8-rest)
9. [Lab_9](#markdown-header-lab-9-responsywne)
10. [Lab_10](#markdown-header-lab-10-progresywne)
11. [Lab_11](#markdown-header-lab-11-gniazda)
12. [Lab_12](#markdown-header-lab-12-autoryzacja)
13. [Lab_13](#markdown-header-lab-13-kolejka)
14. [Lab_14](#markdown-header-lab-14-kolejka-jeszcze-raz)

## Lab 1 Docker
### Praca z gotowymi obrazami
1. Proszę ściągnąć i zainstalować maszynę wirtualną zgodnie z instrukcją umieszczoną w iSOD-zie.
2. Z poziomu maszyny wirtualnej proszę wykonać następujące polecenia. Proszę przeanalizować wyniki prezentowane przez te polecenia.

```console
[rancher@rancher ~]$ docker images -a
[rancher@rancher ~]$ docker hello-world
[rancher@rancher ~]$ docker images -a
```
```console
[rancher@rancher ~]$ docker ps -a
[rancher@rancher ~]$ docker container rename gracious_franklin my_hello_world
[rancher@rancher ~]$ docker rm my_hello_world
[rancher@rancher ~]$ docker ps -a
```
```console
[rancher@rancher ~]$ docker images
[rancher@rancher ~]$ docker rmi hello_world
[rancher@rancher ~]$ docker images
```
```console
[rancher@rancher ~]$ docker container ls
[rancher@rancher ~]$ docker container ls --all
[rancher@rancher ~]$ docker container ls -aq
```
```console
[rancher@rancher ~]$ docker run -it ubuntu /bin/bash
root@ac3fd8973f:/# ls
root@ac3fd8973f:/# exit
[rancher@rancher ~]$ docker ps -a
[rancher@rancher ~]$ docker rename thirsty_mcnulty moje_ubuntu
[rancher@rancher ~]$ docker ps -a
[rancher@rancher ~]$ docker start moje_ubuntu
[rancher@rancher ~]$ docker stats
[rancher@rancher ~]$ docker ps
[rancher@rancher ~]$ docker stop moje_ubuntu
[rancher@rancher ~]$ docker ps
[rancher@rancher ~]$ docker start -i moje_ubuntu 
root@ac3fd8973f:/# exit
[rancher@rancher ~]$ docker rm moje_ubuntu
[rancher@rancher ~]$ docker rmi ubuntu
```

3. Proszę uruchomić serwer `ngingx` w lekkiej wersji (`alpine`) na maszynie wirtualnej. Proszę uzyskać połączenie do serwera z poziomu hosta (w laboratorium: Ubuntu) -- w tym celu należy skorzystać z przeglądarki lub polecenia `curl`. W maszynie wirtualnej należy przekierować port `80` (gościa) na `8080` (hosta).

```console
[rancher@rancher ~]$ docker run --rm --name moj_nginx -p80:80 nginx-alpine
```

- W przeglądarce (host) należy wpisać adres: `localhost:8080`. Jeśli wszystko przebiegło pomyślnie, powinna ukazać się strona powitalna: `Welcome to nginx!`.

### Dockerfile -- własne obrazy
1. Należy przygotować nowy katalog (np. `static_nginx`).
2. Proszę stworzyć strukturę plików / katalogów

```bash
./static_nginx
├── first_app
│   ├── first_static_app.py
│   └── requirements.txt
└── Dockerfile
```

3. Zawartość Dockerfile i innych plików powinna być identyczna jak zaprezentowana w repozytorium (`lab1`). Po stworzeniu powyższej struktury, należy wykonać poniższe polecenia.

```console
[rancher@rancher ~]$ docker build -t=first_flask_app .
[rancher@rancher ~]$ docker images
[rancher@rancher ~]$ docker run --rm --name my_flask_app -p80:80 first_flask_app
```

4. W przeglądarce należy sprawdzić, czy "wszystko" działa zgodnie z oczekiwaniami (na początku lista imion powinna być pusta). `http://localhost:8080/namesApi/`
5. Z poziomu konsoli (hosta) próbujemy wysłać żądanie POST dodające nowe imię.

```console
user@ubuntu:$ curl -X POST localhost:8080/namesApi/ -d "name=Pawel"
```

6. Sprawdzamy, z poziomu konsoli, czy lista imion zwiększyła się o nową wartość.

```console
user@ubuntu:$ curl -X GET localhost:8080/namesApi/
```

7. Próbujemy wyświetlić wszystkie nagłówki żądania (i odpowiedzi) GET. Zwracamy uwagę na `Content-Type` oraz `Content-Length` i próbujemy ustalić, skąd to się wzięło.

```console
user@ubuntu:$ curl -v localhost:8080/namesApi/
```

### SCP Przekierowanie portów 22 na 2222
1. Pliki tworzone na maszynie laboratoryjnej (host: `Ubuntu`) można przesłać do maszyny wirtualnej (`RancherOS`) za pomocą polecenia `SCP`.
Należy z poziomu maszyny wirtualnej ustawić hasło dla użytkownika `rancher`.

```console
user@ubuntu:$ sudo /bin/bash
user@ubuntu:$ passwd rancher
```

2. Następnie, z poziomu hosta (`Ubuntu`), można przesłać pliki i katalogi do wskazanego miejsca. Poniższe polecenie skopiuje całą katalogu (z zawartością) `static_nginx` z maszyny hosta, do katalogu `~/lab1` użytkownika `rancher` z maszyny wirtualnej.

```console
user@ubuntu:$ scp -rp -P 2222 static_nginx rancher@localhost:./lab1
```

## Lab 2 HTML
Zadaniem jest stworzenie podstawowej struktury witryny internetowej. W ramach tego ćwiczenia:

- powinna powstać strona (belka nawigacyjna `<nav>`, 1 sekcja `<section>`, 3 artykuły `<article>`, stopka `<footer>`);
- plik CSS definiujący układ (style) treści;
- plik JS, który będzie ingerował w treść strony (HTML).

### Struktura plików / katalogów
1. Proszę stworzyć poniższą strukturę na maszynie "gospodarza" (Ubuntu).

```bash
lab_2
├── Dockerfile
└── static_html
    ├── index.html
    ├── registration.html
    ├── scripts
    │   ├── registration.js
    │   └── script.js
    └── styles
        ├── style.css
        └── style_new.css
```

### HTML (`index.html`)
1. Struktura strony powinna wyglądać następująco:

```html
<!DOCTYPE html>
<html lang="pl">
    <head></head>
    <body>
        <header>
            <h2></h2>
        </header>
        <nav></nav>
        <section>
            <h3></h3>
            <p></p>
        </section>
        <article>
            <h3></h3>
            <p></p>
        </article>
        <article>
            <h3></h3>
            <p></p>
        </article>
        <article>
            <h3></h3>
            <p></p>

            <table>
            </table>

        </article>

        <div>Zwariowany div</div>

        <footer>
        </footer>
    </body>
</html>
```

2. Znaczniki `<head></head>` należy uzupełnić informacją o tytule strony (wyświetlanym na pasku przeglądarki) oraz informacją o kodowaniu zapewniającym poprawne wyświetlanie polskich znaków diakrytycznych.

```html
<head>
    <title>Bib-maker</title>
    <meta charset="utf-8">
</head>
```

3. Znaczniki `<header></header>` należy uzupełnić nagłówkiem `<h2>` z treścią sprawdzającą poprawność kodowania (używamy najkrótszej zbitki słów zawierajej wszystkie polskie znaki diakrytyczne: "zażółć gęślą jaźń").

4. Znaczniki nawigacyjne `<nav></nav>` wypełniamy w poniższy sposób (tworzymy trzy odnośniki, które obecnie nie działają -- przenoszą nas do bieżącej strony).

```html
<nav>
    <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">Log in</a></li>
        <li><a href="#">Sign up</a></li>
    </ul>
</nav>
```

5. Zawartość paragrafów (`<p></p>`) wewnątrz `<article></article>` i `<section></section>` należy uzupełnić treścią [*Lorem Ipsum*](https://www.lipsum.com/)
Zawartość nagłówków (`<h2></h2>` lub `<h3></h3>`) należy uzupełnić własną treścią.

6. Znaczniki `<table></table>` w ostatnim artykule (`<article></article>`) powinny zawierać nagłówki tabeli oraz treść tabeli. Tabela powinna mieć dwie kolumny odpowiadające liczbie porządkowej i nazwie funkcjonalności udostępnianienej przez tworzoną aplikację (witrynę internetową).
Przykładowa zawartość została zaprezentowana poniżej.

```html
<table>
    <caption>Dostępne funkcjonalności</caption>
    <thead>
        <tr>
            <td>Lp.</td>
            <td>Nazwa</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.</td>
            <td>Uruchamianie za pomocą jednej komendy</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Wyświetlenie statycznej strony powitalnej</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Przejście do strony formularza rejestracji nowego użytkownika</td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Asynchroniczna weryfikacja dostępności nowego loginu</td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Wykonanie prawidłowej rejestracji nowego użytkownika</td>
        </tr>
    </tbody>
</table>
```

7. Zawartość znaczników `<footer></footer>` należy uzupełnić dwoma paragrafami (**inteligentnie podmieniając** przykładową treść).

```html
<p class="footer-inline left">&copy; 2019 Hans Castorp</p>
<p class="footer-inline right">proudly powered by <img src="http://icons.iconarchive.com/icons/bokehlicia/captiva/16/vim-icon.png" alt="Vim icon" title="Vim" class="footer-img"/></p>
```

### CSS (`style.css`)
1. Należy zadbać o "ładną" czcionkę wyświetlanych treści. Dlatego (jako przykładowy wybór) w pliku `index.html` wewnątrz znaczników `<head></head>` należy dopisać (druga linia i trzecia linia odpowiadają za pobranie wybranej czcionki):

```html
<link href="styles/style.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Anonymous+Pro&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
```

Aby czcionka została użyta w przeglądarce, należy uzupełnić plik `style.css` wpisem:

```html
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif !important;
    padding-bottom: 46px; /* the same value as footer height */
}
```
Dodatkowo, za jednym zamachem, ustalimy maksymalną szerokość strony (`1024 px`) i jej wyśrodkowanie względem okna przeglądarki.

2. Aby zapewnić poziome wyświetlanie belki nawigacyjnej, należy zmodyfikować domyślne ustawienia, wykorzystując poniższe wpisy w pliku `style.css`:

```css
nav ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    background-color: beige;
}

nav ul li {
    display: inline;
}

nav ul li a {
    display: inline-block;
    width: 70px;
    text-align: center;
    text-decoration: none;
    color: olive;
}

nav ul li a:hover {
    background-color: #ebda9e;
}
```
3. Wykorzystując deweloperskie narzędzia przeglądarki, proszę sprawdzić znaczenie i / lub działanie poszczególnych selektorów i ich właściwości.
4. Dla poprawy wyglądu stopki strony, należy dopisać następujące style:

```css
footer {
    position: fixed;
    bottom: 0;
    background-color: beige;
    max-width: inherit;
    width: 100%;
    height: 46px;
    font-family: 'Anonymous Pro', sans-serif;
    font-size: 15px;
}

.footer-inline {
    display: inline-block;
    width: 48%;
    padding-left: 1%;
}

.left {
    text-align: left;
}

.right {
    text-align: right;
}

.footer-img {
    vertical-align: middle;
}
```

Dodatkowo, wewnątrz stopki, należy uzupełnić znaczniki `<p></p>` klasami CSS:
- pierwszy paragraf: `class="footer-inline left"`;
- drugi paragraf: `class="footer-inline right"`.

5. Dla nagłówków sekcji i artykółów oraz paragrafów wewnątrz wymienionych znaczników należy ustalić kolory treści. Dla ustalenia uwagi załóżmy, że nagłówek sekcji powinien być ciemno-niebieski, a treść powinna być niebieska. Analogicznie (z kolorem czerwonym) należy postąpić w przypadku artykułu. Dodatkowym wymaganiem jest uzyskanie wcięcia akapitowego (w pierwszym wierszu).
Aby uzyskać opisany efekt, w pliku `style.css` należy dopisać:

```css
section p,
article p {
    text-indent: 25px;
}

section h3 {
    color: darkblue;
}

section p {
    color: blue;
}

article h3 {
    color: darkred;
}

article p {
    color: red;
}
```

6. Zadaniem związanym z tabelą jest ustalenie dla niej obramowania oraz pokolorowanie wierszy tabeli naprzemiennie. Aby to osiągnąć, należy wykorzystać specjalne właściwości selektorów: `tr`, `td`.
Kod realizujący opisane wymaganie został przedstawiony poniżej.

```css
table {
    margin-left: auto;
    margin-right: auto;
}

table, th, td {
    border: 1px solid beige;
}

.caption-bottom {
    caption-side: bottom;
}

td:nth-child(1) {
    text-align: right;
}

tr:nth-child(odd) {
    background-color: #fbda9e
}

tr:nth-child(even) {
    background-color: #dbda9e;
}

thead tr {
    background-color: #dbda9e !important;
}
```

Proszę zwrócić uwagę na słowo kluczowe `!important`. Proszę, z poziomu przeglądarki (z narzędzi deweloperskich), wyłączyć warunek `!important` (usunąć z kodu).

### JS (`script.js`)
1. Aby móc wykonywać kod `JavaScript` z pliku `script.js`, należy wewnątrz znaczników `<head></head>` dopisać:

```html
<script src="scripts/script.js"></script>
```

2. Pierwszym krokiem jest wypisanie w konsoli przeglądarki kilku wartości w pętli. W tym celu zawartość pliku `script.js` należy uzupełnić o kod:

```js
var a = 3;
for (var i = 0; i < a; i++) {
    console.log(i);
}
```

Efekt działania kodu powinien być widoczny w konsoli, po odświeżeniu strony.

3. Drugim zadaniem jest wypisanie w pętli zawartości tablicy z imionami. Dlatego też plik `script.js` uzupełniamy kodem:

```js
var names = ["Wiktor", "Paweł", "Zuza", "Bartek", "Robert", "Jacek"];
names.forEach(function (name, id) {
    console.log(name + id);
});
```

Weryfikację działania należy przeprowadzić w sposób analogiczny do poprzedniego punktu.

4. Ostatnim krokiem jest dopisanie kodu, który tabelę (`<table></table>`) będzie modyfikował poprzez dopisanie kolejnych wierszy z imionami (umieszczonymi w poprzednim punkcie).

```js
document.addEventListener('DOMContentLoaded', function (event) {
    names.forEach(putNamesIntoTable);
});

function putNamesIntoTable(name) {
    var tbodys = document.getElementsByTagName("tbody");
    var size = (tbodys[0]).children.length;
    (tbodys[0]).innerHTML += "<tr><td>" + (size + 1) + "</td><td>" + name + "</td></tr>";
}
```

### HTML (`registration.html`)
1. Tworzymy stronę z formularzem wykorzystywanym przy rejestracji. Struktura strony powinna być zbliżona do `index.html`. Różnica jest taka, że wewnątrz znaczników `<body></body>` znajduje się tylko jedna sekcja.

```html
<section>
    <h3>Zarejestruj się</h3>

    <form id="registration-form" class="registration-form">
        <label for="firstname">Imię</label>
        <input id="firstname" type="text" name="firstname"/>
        <br/>

        <label for="surname">Nazwisko</label>
        <input id="surname" type="text" name="surname"/>
        <br/>

        <label for="email" class="required">E-mail</label>
        <input id="email" type="text" name="email"/>
        <br/>

        <label for="password" class="required">Hasło</label>
        <input id="password" type="text" name="password"/>
        <br/>

        <label for="repeat-password" class="required">Powtórz hasło</label>
        <input id="repeat-password" type="text" name="repeat-password"/>
        <br/>

        <input id="button-reg-form" type="submit" value="Rejestruj"/>
    </form>
</section>
```

Wenwątrz znaczników `<head></head>` należy wstawić odnośnik do innego pliku `JS`.
```html
<script src="scripts/registration.js"></script>
```

### CSS (`style.css`)
1. Dla formularza rejestracji należy zdefiniować wykorzystywane (w powyższym kodzie) klasy. Poniżej umieszczone klasy odpowiadają za "ładne" ułożenie pól formularza. **Warto zwrócić uwagę** na selektor `label` z klasą `required`.

```css
.registration-form {
    width: 400px;
    margin-left: auto;
    margin-right: auto;
}

.registration-form label {
    float: left;
    width: 150px;
    text-align: right;
}

.registration-form label::after {
    content: ":";
}

.registration-form input {
    float: left;
    width: 150px;
}

#button-reg-form {
    float: none;
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 100px;
}

label.required::after {
    color: red;
    content: "*:";
}
```

### JS (`registration.js`)
1. Należy napisać kod, który będzie przechwytywał każdą próbę wysłania formularza rejestracji. W konsoli powinny pojawić się wartości z uzupełnionych pól formularza.

```js
document.addEventListener('DOMContentLoaded', function (event) {

    var registrationForm = document.getElementById("registration-form");

    registrationForm.addEventListener("submit", function (event) {
        event.preventDefault();

        console.log("zatrzymałem");

        var n = event.srcElement.length;
        for (var i = 0; i < n; i++) {
            console.log(event.srcElement[i].value);
        }

    });
});
```

### CSS (`style_new.css`)
1. Z ciekawości można sprawdzić, jak zachowają się nasze style jeśli pomiędzy znacznikami `<head></head>` (w `index.html`) zostanie załączony (jako drugi) plik `style_new.css`. Proszę "nadpisać" jakiś styl, a później w pierwszym pliku (`style.css`) wykorzystać słowo kluczowe `!important`, aby zabezpieczyć się przed nadpisaniem tego stylu.

```html
<link href="styles/style.css" rel="stylesheet" type="text/css">
<link href="styles/style_new.css" rel="stylesheet" type="text/css">
```

2. Prosze przeanalizować różne wartości atrybutu `position`. W tym celu, w pliku `index.html` należy uzupełnić (jedyny) znacznik `<div></div>` o klasę `my-example-div`.

```html
<div class="my-example-div">Zwariowany div</div>
```

Następnym krokiem jest "uruchomienie" (zakomentowanie / odkomentowanie) kolejnych wersji tej klasy w pliku `style_new.css`.

```css
.my-example-div {
    position: fixed;
    top: 0;
    right: 0;
    width: 90px;
    height: 90px;
    background-color: red;
}
/*
.my-example-div {
    position: relative;
    top: 0;
    right: 0;
    width: 90px;
    height: 90px;
    background-color: red;
}

.my-example-div {
    position: absolute;
    top: 0;
    right: 0;
    width: 90px;
    height: 90px;
    background-color: red;
}

.my-example-div {
    position: sticky;
    top: 0;
    right: 0;
    width: 90px;
    height: 90px;
    background-color: red;
}*/
```

### Walidatory (HTML, CSS)
Poprawność kodów źródłowych należy sprawdzić walidatorami z W3C.

- `https://validator.w3.org/`;
- `http://jigsaw.w3.org/css-validator/`.

### Uruchomienie (docker run)

```console
[rancher@rancher ~]$ mkdir lab_2
```
```console
user@ubuntu:$ scp -P2222 -rp ./* rancher@localhost:./lab_2
```
```console
[rancher@rancher ~]$ cd ~/lab_2
[rancher@rancher ~]$ docker build -t=my_nginx_app .
[rancher@rancher ~]$ docker run --rm -it -p80:80 my_nginx_app
```


## Lab 3 AJAX
1. W trakcie trzecich zajęć należy dokończyć (i zmodyfikować) formularz rejestracyjny, aby był zgodny z polami wymaganymi przez `endpoint` wystawiony pod adresem `https://pi.iem.pw.edu.pl/`.
2. Obraz wytawiony pod wstakaznym adresem można pobrać (z Docker Hub) i uruchomić lokalnie poleceniem:
```bash
[rancher@rancher ~]$ docker run --rm -it -p5000:5000 chaberb/register
```

3. Próbę rejestracji użytkownika można wykonać za pomocą `curl`. Proszę zwrócić uwagę na parametr `-k` oraz na nagłówek `Content-Type`.
```bash
curl -k -X POST \
  https://pi.iem.pw.edu.pl/register \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data; boundary=----MyBoundary1234567890' \
  -F firstname=Jasio \
  -F lastname=Ka \
  -F password=abcdefgh \
  -F birthdate=2019-10-10 \
  -F login=jasiok \
  -F pesel=57092246652 \
  -F sex=M \
  -F 'photo=@/sciezka/do/mojego/pliku/ze/zdjeciem.png'
```
4. Do generowania "prawidłowych" wartości PESEL można skorzystać z `http://generatory.it/`.
5. Proszę popróbować różnych kombinacji zapytań wysyłanych przez program Postman.
6. Nowy formularz rejestracji powinien wyglądać podobnie do prezentowanego poniżej

```html
<section>

    <h3>Zarejestruj się</h3>

    <form id="registration-form" class="registration-form">

        <label for="firstname" class="required">Imię</label>
        <input id="firstname" type="text" name="firstname"/>
        <br/>

        <label for="surname" class="required">Nazwisko</label>
        <input id="surname" type="text" name="surname"/>
        <br/>

        <label for="birthdate" class="required">Data urodzenia</label>
        <input id="birthdate" type="text" name="birthdate" placeholder="YYYY-MM-DD"/>
        <br/>

        <label for="pesel" class="required">PESEL</label>
        <input id="pesel" type="text" name="pesel"/>
        <br/>

        <label for="sex" class="required">Płeć</label>
        <input id="sex" type="text" name="sex"/>
        <br/>

        <label for="login" class="required">Login</label>
        <input id="login" type="text" name="login"/>
        <br/>

        <label for="password" class="required">Hasło</label>
        <input id="password" type="password" name="password"/>
        <br/>

        <label for="repeat-password" class="required">Powtórz hasło</label>
        <input id="repeat-password" type="password" name="repeat-password"/>
        <br/>

        <label for="avatar" class="required">Avatar</label>
        <input id="avatar" type="file" name="avatar">
        <br/>

        <input id="button-reg-form" type="submit" value="Rejestruj"/>               

    </form>

</section>
```

7. Proszę wykorzystać poniższy kod jako `registration.js`. Proszę przeanalizować (i poprawić) jego działanie.
```js
document.addEventListener('DOMContentLoaded', function (event) {

    const GET = "GET";
    const POST = "POST";

    let registrationForm = document.getElementById("registration-form");
    let loginInput = document.getElementById("login");

    loginInput.addEventListener("change", checkLoginAvailability);

    registrationForm.addEventListener("submit", function (event) {
        event.preventDefault();
        console.log("zatrzymałem");

        var n = event.srcElement.length;
        for (var i = 0; i < n; i++) {
            console.log(event.srcElement[i].value);
        }
    });

    function checkLoginAvailability() {
        let loginInput = document.getElementById("login");
        let baseUrl = "https://pi.iem.pw.edu.pl/user/";
        let userUrl = baseUrl + loginInput.value;

        fetch(userUrl, {method: GET}).then(function (resp) {
            console.log(resp.status);
            return resp.status;

        }).catch(function (err) {
            console.log(err);
            return err.status;
        });
    }
});
```

### Obietnica (`Promise`)

Modyfikując powyższy kod, można dojść do sytuacji, w której pojawi się potrzeba wywołania funkcji zwrotnej. Ponieważ `JavaScript` ze swojej natury działa asynchronicznie, to oznacza, że kod nie będzie czekał (stał w miejscu) na wiadomość zwrotną z funkcji, która nie zgłosi od razu odpowiedzi. Aby obsłużyć taką sytuację, można korzystać z funkcji zwrotnych (`callback`) lub ich współczesnych modyfikacji -- obietnic (`promise`).

#### Resolve || Reject

Obietnica składa się z dwóch podstawowych zachowań. Pierwsze (`resolve`) określa, co ma się stać, gdy kod asynchroniczny wykona się poprawnie (co mamy zwrócić). Drugie (`reject`) określa, jakie ma być zachowanie, gdy asynchroniczny kod wykona się niepoprawnie (czyli co powinniśmy zwrócić do funkcji, która oczekuje na wiadomość zwrotną).

1) Zmodyfikujmy powyższy kod (w całości, ale najpierw pokażę fragmenty) o dodatkowe stałe i zmienne.

```js
const GET = "GET";
const POST = "POST";
const URL = "https://pi.iem.pw.edu.pl/";

const LOGIN_FIELD_ID = "login";

var HTTP_STATUS = {OK: 200, NOT_FOUND: 404};
```

2) Dodajmy dwie funkcje zwracające obietnice. Funkcja `checkLoginAvailability` ze względu na specyfikę odpowiedzi serwera (serwer zwraca kody odpowiedzi:  `200`,  `404`) ma tylko implementację instrukcji `resolve`. Należy się zastanowić, czy implementacja `reject` byłaby (bardziej) poprawna w tej sytuacji.

Funkcja `isLoginAvailable` może zwrócić `true`, `false` lub rzucić wyjątkiem (`throw`).

```js
function isLoginAvailable() {
   return Promise.resolve(checkLoginAvailability().then(function (statusCode) {
       if (statusCode === HTTP_STATUS.OK) {
           return false;

       } else if (statusCode === HTTP_STATUS.NOT_FOUND) {
           return true

       } else {
           throw "Unknown login availability status: " + statusCode;
       }
   }));
}

function checkLoginAvailability() {
   let loginInput = document.getElementById(LOGIN_FIELD_ID);
   let baseUrl = URL + "user/";
   let userUrl = baseUrl + loginInput.value;

   return Promise.resolve(fetch(userUrl, {method: GET}).then(function (resp) {
       return resp.status;
   }).catch(function (err) {
       return err.status;
   }));
}
```

3) Ponieważ poprzednio omawiana funkcja () może zwrócić trzy "odpowiedzi" (`true`, `false`, `exception`), to funkcja ją wywołująca powinna być przygotowana na takie wartości (`catch`). Dlatego proszę przeanalizować poniższy kod (należy zwrócić uwagę na wołanie `then` obsługujące wynik `resolve` i `catch` obsługujące rzucony wyjątek).

```js
function updateLoginAvailabilityMessage() {
   let warningElemId = "loginWarning";
   let warningMessage = "Ten login jest już zajęty";

   isLoginAvailable().then(function (isAvailable) {
       if (isAvailable) {
           console.log("Available login!");
           removeLoginWarningMessage(warningElemId);
       } else {
           console.log("NOT available login");
           showLoginWarningMessage(warningElemId, warningMessage);
       }
   }).catch(function (error) {
       console.error("Something went wrong while checking login.");
       console.error(error);
   });
}
```

4) Funkcjami pomocniczymi do wyświetlania / ukrywania (kasowania) komunikatu o zajętym loginie mogą być poniższe dwa przykłady.

**Usuwanie komunikatu**

```js
function removeLoginWarningMessage(warningElemId) {
   let warningElem = document.getElementById(warningElemId);

   if (warningElem !== null) {
       warningElem.remove();
   }
}
```

**Dodawanie komunikatu**

```js
function showLoginWarningMessage(newElemId, message) {
   let warningElem = prepareLoginWarningElem(newElemId, message);
   appendAfterElem(LOGIN_FIELD_ID, warningElem);
}
```

```js
function prepareLoginWarningElem(newElemId, message) {
   let warningField = document.getElementById(newElemId);

   if (warningField === null) {
       let textMessage = document.createTextNode(message);
       warningField = document.createElement('span');

       warningField.setAttribute("id", newElemId);
       warningField.className = "warning-field";
       warningField.appendChild(textMessage);
   }
   return warningField;
}
```

Proszę zwrócić uwagę w powyższej funkcji ```prepareLoginWarningElem``` na instrukcję ```warningField === null```. JavaScript rozróżnia porównanie zgodności wartości (tekstu) od zgodności z typem i wartością. Innymi słowy `==` pozwoli porównać liczbę `2` z literałem łańcuchowym (napisem) `"2"` jako równe. Natomiast operator `===` będzie porównywał również typ (nie będzie próbował zamieniać liczby `2` na napis `"2"`).

Brakuje jeszcze funkcji odpowiedzialnej za wstawienie tekstu zaraz za polem `input` przewidzianym na login.

```js
function appendAfterElem(currentElemId, newElem) {
   let currentElem = document.getElementById(currentElemId);
   currentElem.insertAdjacentElement('afterend', newElem);
}
```

5) Aby wszystko (względnie) pięknie wyglądało, należy jeszcze dodać klasę w pliku `style.css`.

```css
.warning-field {
   display: inline-block;
   width: 150px;
   color: red;
   font-size: small;
}
```

6) Cały plik `script.js` może wyglądać tak, jak pokazano poniżej.

```js
document.addEventListener('DOMContentLoaded', function (event) {

   const GET = "GET";
   const POST = "POST";
   const URL = "https://pi.iem.pw.edu.pl/";

   const LOGIN_FIELD_ID = "login";

   var HTTP_STATUS = {OK: 200, NOT_FOUND: 404};

   prepareEventOnLoginChange();

   let registrationForm = document.getElementById("registration-form");

   registrationForm.addEventListener("submit", function (event) {
       event.preventDefault();

       console.log("Form submission stopped.");

       var n = event.srcElement.length;
       for (var i = 0; i < n; i++) {
           console.log(event.srcElement[i].value);
       }
   });

   function prepareEventOnLoginChange() {
       let loginInput = document.getElementById(LOGIN_FIELD_ID);
       loginInput.addEventListener("change", updateLoginAvailabilityMessage);
   }

   function updateLoginAvailabilityMessage() {
       let warningElemId = "loginWarning";
       let warningMessage = "Ten login jest już zajęty";

       isLoginAvailable().then(function (isAvailable) {
           if (isAvailable) {
               console.log("Available login!");
               removeLoginWarningMessage(warningElemId);
           } else {
               console.log("NOT available login");
               showLoginWarningMessage(warningElemId, warningMessage);
           }
       }).catch(function (error) {
           console.error("Something went wrong while checking login.");
           console.error(error);
       });
   }

   function showLoginWarningMessage(newElemId, message) {
       let warningElem = prepareLoginWarningElem(newElemId, message);
       appendAfterElem(LOGIN_FIELD_ID, warningElem);
   }

   function removeLoginWarningMessage(warningElemId) {
       let warningElem = document.getElementById(warningElemId);

       if (warningElem !== null) {
           warningElem.remove();
       }
   }

   function prepareLoginWarningElem(newElemId, message) {
       let warningField = document.getElementById(newElemId);

       if (warningField === null) {
           let textMessage = document.createTextNode(message);
           warningField = document.createElement('span');

           warningField.setAttribute("id", newElemId);
           warningField.className = "warning-field";
           warningField.appendChild(textMessage);
       }
       return warningField;
   }

   function appendAfterElem(currentElemId, newElem) {
       let currentElem = document.getElementById(currentElemId);
       currentElem.insertAdjacentElement('afterend', newElem);
   }

   function isLoginAvailable() {
       return Promise.resolve(checkLoginAvailability().then(function (statusCode) {
           if (statusCode === HTTP_STATUS.OK) {
               return false;

           } else if (statusCode === HTTP_STATUS.NOT_FOUND) {
               return true

           } else {
               throw "Unknown login availability status: " + statusCode;
           }
       }));
   }

   function checkLoginAvailability() {
       let loginInput = document.getElementById(LOGIN_FIELD_ID);
       let baseUrl = URL + "user/";
       let userUrl = baseUrl + loginInput.value;

       return Promise.resolve(fetch(userUrl, {method: GET}).then(function (resp) {
           return resp.status;
       }).catch(function (err) {
           return err.status;
       }));
   }

});
```

## Lab 4 Server Side

Zadaniem na czwartych zajęciach laboratoryjnych jest przygotowanie aplikacji (po stronie serwera), która pozwoli nam na dodawanie nowych obiektów. Zakładamy, że tworzymy bibliotekę. Tym razem jeszcze nie korzystamy z bazy danych. Wszystkie byty biblioteczne będziemy przechowywać w pamięci serwera (zostanie wyczyszczona po wyłączeniu aplikacji).

### Struktura plików / katalogów
1. Proszę stworzyć poniższą strukturę na maszynie "gospodarza" (Ubuntu).

```bash
lab_4
├── Dockerfile
└── second_app
    ├── library_app.py
    ├── requirements.txt
    ├── src
    │   └── book
    │       └── book.py
    ├── static
    │   ├── scripts
    │   │   └── script.js
    │   └── styles
    │       └── style.css
    └── templates
        └── index.html
```

### Dockerfile
```bash
FROM python:3.7-slim
WORKDIR /second_app
COPY ./second_app /second_app
RUN pip install --trusted-host pypi.python.org -r requirements.txt
EXPOSE 80
CMD ["python", "library_app.py"]
```

### Server side (Python)
Kod źródłowy dla klasy `Book` powinien zawierać trzy pola i konstruktor (imię i nazwisko autora, tytuł książki, rok wydania książki).
```python
class Book:
    def __init__(self, author, title, year):
        self.author = author
        self.title = title
        self.year = year
```

Kod źródłowy dla aplikacji (`library_app.py`) powinien posiadać dwa `endpoint`-y.
Pierwszy -- do dodawania książek, drugi -- do pobierania listy wszystkich książek. Dodatkowo pierwszy `endpoint` powinien wyświetlać listę wszystkich książek (autor, tytuł, rok) w tabeli strony HTML.

```python
@app.route("/", methods=["GET"])
def home():
    return render_template("index.html", my_books = books)
```

```python
@app.route("/bookApi/", methods=["POST"])
def add_book():
    book = to_book(request.form)
    books.append(book)
    return jsonify({"author": book.author}), 201
    
def to_book(request):
    return Book(request.get("author"), request.get("title"), request.get("year"))
```

Cały przykład może wyglądać następująco:
```python
from flask import Flask, request
from flask import jsonify, render_template
from src.book.book import Book

app = Flask(__name__, static_url_path = "")

books = []

@app.route("/", methods=["GET"])
def home():
    return render_template("index.html", my_books = books)

@app.route("/bookApi/", methods=["POST"])
def add_book():
    book = to_book(request.form)
    books.append(book)
    return jsonify({"author": book.author}), 201
    
def to_book(request):
    return Book(request.get("author"), request.get("title"), request.get("year"))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
```

### Wyświetlanie (HTML)
Aby wyświetlić listę dodanych książek, można skorzystać ze specjalnej składni interpretowanej przez `Flask`.
```html
<ul>
    {% for book in my_books %}
    <li>{{ book.author }}</li>
    {% endfor %}
</ul>
```

### Dla poprawy wyglądu (CSS)
```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
}
```

### Dla sprawdzenia, że skrypty też działają (JS)
```js
document.addEventListener('DOMContentLoaded', function (event) {
    console.log("Here!");
});
```

### Pełny przykład kodu strony (HTML)
```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Library</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <script src="/scripts/script.js"></script>
    </head>
    <body>
        <header>
            <h2>ZAŻÓŁĆ GĘŚLĄ JAŹŃ</h2>
        </header>
        <section>
            <ul>
                {% for book in my_books %}
                <li>{{ book.author }}</li>
                {% endfor %}
            </ul>
        </section>
    </body>
</html>
```

### Zadania do zrealizowania w ramach zajęć
1) Proszę zbudować obraz aplikacji za pomocą `Dockerfile`.
```bash
[rancher@rancher ~]$ docker build . -t=second_app
```

2) Proszę uruchomić kontener z aplikacją.
```bash
[rancher@rancher ~]$ docker run --rm -p80:80 -i second_app
```
3) Proszę sprawdzić, czy strona poprawnie wyświetla się w przeglądarce `localhost:8080` na maszynie gospodarza (`Ubuntu`).

4) Przy pomocy programu `Postman`, proszę dodać kilka bytów bibliotecznych do zbioru.

5) Powyższy punkt należy powtórzyć zastępując program `Postman` narzędziem `curl`.

6) Proszę zmodyfikować kod (`html`) w taki sposób, aby cała książka (imię i nazwisko autora,  tytuł, rok wydania) była wyświetlana w tabeli (`<table></table>`).

7) Do książki (obiekt `Book`) należy dodać nowe pole (np. liczba stron w książce). Nowe pole również powinno być wyświetlane w tabeli.

8) Proszę pod tabelą umieścić formularz, który pozwoli na dopisywanie nowych książek do listy. Po dodaniu książki, lista powinna się odświeżyć z nowo dodaną pozycją (książką).

## Lab 5 Redis
Celem zajęć jest pierwsze spotkanie z Redis-em (bazą danych typu `klucz:wartość`).
Dodatkową "nowością" będzie poznanie narzędzia `docker-compose` -- pozwala na łatwiejsze budowanie obrazów i zarządzanie wieloma obrazami w bardziej złożonym projekcie.
W ramach obecnych zajęć będziemy wykorzystywać dwa obrazy: "własny" (do aplikacji pisanej w `Flask-u`), obraz `Redis-a` (gotowy obraz, z którego utworzymy kontener).

### Instalacja docker-compose
Maszyny wirtualne (`RancherOS`) nie mają domyślnie zainstalowanego narzędzia `docker-compose`. Aby uzupełnić te braki, należy z poziomu maszyny wirtualnej wykonać poniższe polecenia (przystosujemy [instrukcję ze strony producenta](https://docs.docker.com/compose/install/) do naszych potrzeb i możliwości -- użyjemy polecenia `wget` zamiast `curl`).

```sh
[rancher@rancher ~]$ sudo wget "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -O /usr/bin/docker-compose
[rancher@rancher ~]$ sudo chmod +x /usr/bin/docker-compose
```

Jeżeli wszystko przebiegło pomyślnie, to teraz powinniśmy móc wyświetlić instrukcję dla polecenia
```sh
[rancher@rancher ~]$ docker-compose
```

### Struktura katalogów

Dla dzisiejszego ćwiczenia należy stworzyć drzewo plików / katalogów widoczne poniżej.

```bash
lab_5
├── docker-compose.yml
├── Dockerfile
└── third_app
    ├── files
    │   └── 
    ├── files_app.py
    ├── requirements.txt
    ├── static
    │   └── styles
    │       └── style.css
    └── templates
        └── index.html
```

### docker-compose.yml
Poniższy plik konfiguracyjny dla `docker-compose` zawiera informację o dwóch kontenerach, które powstaną / zostaną uruchomione jednym poleceniem. W pliku zdefiniowany jest kontener `web`, który ma być zbudowany na podstawie pliku konfiguracyjnego `Dockerfile` (świadczy o tym komenda `build .`). Drugim kontenerem jest `redis`, którego gotowy obraz zostanie ściągnięty w wersji `redis:alpine`.

Proszę jeszcze zwrócić uwagę na sekcję `volumes`. Poniższy zapis oznacza, że katalog `./third_app` hosta (`RancherOS`) będzie odpowiadał katalogowi gościa (`Docker`) `/third_app`. Pliki, które będą zapisywane przez aplikację będą również widoczne z poziomu maszyny hosta (`RancherOS`).

Kolejnym istotnym elementem jest `environment`. Zdefiniowane zmienne środowiskowe powodują podobne działanie. `Flask` domyślnie (bez tych ustawień) uruchamia aplikację w trybie produkcyjnym (wtedy wartość `FLASK_DEBUG` jest ustawiona na `false`). W trybie `development` zmienna środowiskowa `FLASK_DEBUG` jest ustawiana na `true`. Innymi słowy wykorzystanie jednej z dwóch poniższych zmiennych wystarczy, aby `Flask` uruchomił aplikację ze statusem `debug on`.
W trybie `debug on` można na żywo wprowadzać zmiany w kodzie -- po zapisaniu zmiany w pliku, kod zostanie wykonany jeszcze raz (automatycznie). Jest to bardzo pomocna funkcjonalność, gdy pracuje się nad oprogramowaniem małymi krokami (wprowadzając częste zmiany).

Warto jeszcze wspomnieć o mapowaniu portów `80:80`. Pierwsza wartość oznacza port hosta (`RancherOS`), druga -- port gościa (`Docker`).
Maszyna wirtualna `RancherOS` mapuje port `80` gościa (`RancherOS`) na port `80` hosta (`Ubuntu`).
Parametr `version` oznacza używaną / deklarowaną wersję  `docker-compose`.

```bash
version: '3'
services:
  web:
    build: .
    ports:
      - "80:80"
    volumes:
      - ./third_app:/third_app
    environment:
      FLASK_ENV: development
      FLASK_DEBUG: 1
  redis:
    image: "redis:alpine"
```

### Dockerfile
Poniższy plik konfiguracyjny (`Dockerfile`) dla obrazu pod aplikację pisaną w `Flask-u` wygląda bardzo podobnie do innych, prezentowanych podczas poprzednich zajęć. Skomentowania wymagają definicje zmiennych środowiskowych.
Jeżeli w `files_app.py` zdefiniujemy standardową instrukcję do uruchamiania aplikacji pod konkretnym adresem i portem 

```py
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
```

to w takiej sytuacji definicja zmiennych środowiskowych (`FLASK_APP`,`FLASK_RUN_HOST`,`FLASK_RUN_PORT`) jest redundantna. A aplikacja jest uruchamiana dzięki konfiguracji `CMD ["python", "files_app.py"]`.
Jeżeli jednak w `files_app.py` nie będzie powyższej instrukcji warunkowej `if`, to wtedy konieczne jest zdefiniowanie wspomnianych zmiennych środowiskowych, a aplikację należy uruchomić konfiguracją `CMD ["flask", "run"]`.

```bash
FROM python:3.7-alpine
WORKDIR /third_app
ENV FLASK_APP files_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80
COPY ./third_app /third_app
RUN pip install -r requirements.txt
CMD ["python", "files_app.py"]
```

### Requirements
Plik `requirements.txt` zawiera informacje o bibliotekach, które będą zainstalowane instrukcją z pliku konfiguracyjnego (`RUN pip install -r requirements.txt`).

```
flask
redis
```

### files_app
Poniżej został umieszczony kod źródłowy (`files_app.py`), który w swojej prostej wersji wystawia trzy `endpoint-y`.
Po zbudowaniu obrazów (`docker-compose build`) i uruchomieniu kontenerów (`docker-compose up`), po wejściu pod adres `localhost:8080` powinna zostać wygenerowana strona główna z formularzem dodawania nowych plików (pliki mają być wyświetlane poniżej formularza dodawania nowych plików).

Proszę przeanalizować (przed uruchomieniem) cały kod. Proszę zwrócić uwagę, iż w przykładzie jest pozostawiona instrukcja warunkowa `if` do uruchamiania aplikacji. Nie jest ona potrzebna, jeżeli w `Dockerfile` zdefiniowano `CMD ["flask", "run"]` (a w `docker-compose.yml` zdefiniowano zmienną środowiskową `FLASK_APP` wskazującą na plik `files_app.py`).

```py
from flask import Flask, request, render_template, redirect, url_for, send_file
import redis
import sys

app = Flask(__name__, static_url_path = "")
db = redis.Redis(host='redis', port=6379, decode_responses=True)

DIR_PATH = "files/"
FILE_COUNTER = "file_counter"
ORG_FILENAME = "org_filename"
NEW_FILENAME = "new_filename"
PATH_TO_FILE = "path_to_file"
FILENAMES = "filenames"

@app.route('/')
def show_articles():
    files = db.hvals(FILENAMES)
    return render_template("index.html", my_files = files)

@app.route("/article-manager", methods=["POST"])
def upload_article():
    f = request.files["article"]
    save_file(f)
    return redirect(url_for("show_articles"))

@app.route("/article-manager/<string:article_hash>", methods=["GET"])
def download_article(article_hash):
    full_name = db.hget(article_hash, PATH_TO_FILE)
    org_filename = db.hget(article_hash, ORG_FILENAME)
    
    if(full_name != None):
        try:
            return send_file(full_name, attachment_filename = org_filename)
        except Exception as e:
            print(e, file = sys.stderr)

    return org_filename, 200

def save_file(file_to_save):
    if(len(file_to_save.filename) > 0):
        filename_prefix = str(db.incr(FILE_COUNTER))
        new_filename = filename_prefix + file_to_save.filename
        path_to_file = DIR_PATH + new_filename
        file_to_save.save(path_to_file)

        db.hset(new_filename, ORG_FILENAME, file_to_save.filename)
        db.hset(new_filename, PATH_TO_FILE, path_to_file)
        db.hset(FILENAMES, new_filename, file_to_save.filename)
    else:
        print("\n\t\t[WARN] Empty content of file\n", file = sys.stderr)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
```

### HTML (index.html)
Proszę zwrócić uwagę na zaszycie (w kodzie formularza) adresu do `endpoint-a` `article-manager` -- to nie jest najlepsze rozwiązanie. Dlaczego? Proszę spróbować odpowiedzieć na to pytanie. Proszę również przemyśleć, jaką zmianę można / należy wprowadzić.

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>File uploader</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
            <h2>"Filmuj rzeź żądań, pość, gnęb chłystków!"</h2>
            <p>Bo nie może być ciągle: "zażółć gęślą jaźń".</p>
        </header>
        <section>
            <form action = "http://localhost:8080/article-manager" method = "POST" enctype = "multipart/form-data">
                <input type = "file" name = "article" />
                <input type = "submit" value="Dodaj plik"/>
            </form>

            <ul>
                {% for file in my_files %}
                <li>{{ file }}</li>
                {% endfor %}
            </ul>
        </section>
    </body>
</html>
```

### CSS (style.css)
Dodajemy "standardowy" styl, aby strona wyglądała nieco ładniej.

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
}
```

### Uruchomienie

#### Pierwsze
Pierwsze uruchomienie aplikacji zdefiniowanej z pomocą `docker-compose.yml` należy wykonać poleceniem `docker-compose build`. Można zamiennie (wyjątkowo) użyć `docker-compose up`, ponieważ `Docker` wykryje, że wymagane obrazy jeszcze nie istnieją (nie można podnieść kontenerów), więc trzeba je zbudować.

#### Kolejne
`docker-compose up` lub `docker-compose up --build`

#### W tle
Można uruchomić kontenery, aby pracowały w tle (konsola nie będzie wyświetlać aktualnego stanu i logów). Taki efekt można uzyskać poleceniem `docker-compose up -d`.

### Kończenie pracy
Pracę z uruchomionymi kontenerami można zakończyć poprzez kombinację klawiszy `CRTL+C` lub poleceniem `docker-compose stop` (ewentualnie `docker-compose down`).

### Zadania do zrealizowania w ramach zajęć
1) Proszę zmodyfikować (pod swoje porzeby) przykładowy kod, aby możliwe było pobieranie przesłanych plików.

2) Proszę zapewnić możliwość wysyłania wielu plików o takiej samej nazwie.

3) Proszę odpowiedzieć na pytania pojawiające się w opisie (w sekcji "HTML (index.html)").

4) Proszę zabezpieczyć kod przed błędami (np. podczas pobierania nieistniejącego pliku).

5) Proszę przejść tutorial z obsługi bazy danych [`Redis`](https://try.redis.io/).

## Lab 6 Redis and session
Celem kolejnych zajęć laboratoryjnych jest utrwalenie / rozszerzenie wiadomości na temat bazy danych `Redis` oraz przygotowanie aplikacji pozwalającej na logowanie się użytkowników do systemu.
W pierwszej części zajmiemy się konfiguracją niezależnej instancji bazy danych `Redis`.

### Redis, docker
Obrazy `Docker-a` mają przygotowane konfiguracje bazy danych `Redis`. W ramach przeprowadzanego ćwiczenia wykorzystamy jeden z takich gotowych obrazów.

#### Uruchomienie serwera bazy danych
Serwer bazy `Redis` domyślnie działa na porcie `6379`. Obraz `docker-owy`, z którego skorzystamy, nazywa się tak samo jak baza danych: `Redis`. Kontener ze wspomnianym obrazem również nazwiemy tak samo: `redis`. Całość uruchomimy w trybie `detach` (parametr `-d`) -- czyli kontener z serwerem będzie działać w tle. Opisany rezultat uzyskamy wykonując polecenie:

```bash
[rancher@rancher ~]$ docker run --rm --name redis -d -p 6379:6379 redis
```

Warto zwrócić uwagę na parametr wywołania `--rm` -- gdy zatrzymamy serwer poleceniem `docker stop redis`, kontener zostanie usunięty. Jeżeli nie dodamy wspomnianego parametru (`--rm`), to kontener należy usunąć poleceniem: `docker rm redis`.

#### Uruchomienie wiersza poleceń
Gdy serwer bazy danych `Redis` jest już uruchomiony, to można się do niego podłączyć w trybie interaktywnym (z konsolą), wykonując polecenie `redis-cli` na kontenerze o nazwie `redis`. Całe polecenie pokazano poniżej.

```bash
[rancher@rancher ~]$ docker exec -it redis redis-cli
```

#### Praca bezpośrednio na bazie danych
Po uruchomieniu `redis-cli` należy przetestować tworzenie: pojedynczych wpisów (klucz:wartość), list, (nieposortowanych) zbiorów, (posortowanych) zbiorów, map.

##### Redis -> klucz:wartość
###### Zadanie
Proszę przypisać do klucza `hello` wartość `world`.

```bash
127.0.0.1:6379> set hello world
OK
```

```bash
127.0.0.1:6379> get hello
"world"
```

```bash
127.0.0.1:6379> del hello
(integer) 1
```

```bash
127.0.0.1:6379> get hello
(nil)
```

##### Redis -> lista
###### Zadanie
Proszę stworzyć listę imion. Proszę dodawać i usuwać imiona. Proszę również wyświetlić wszystkie imiona z listy.

```bash
127.0.0.1:6379> rpush names Pawel Bartek Robert Jacek
(integer) 4
```

```bash
127.0.0.1:6379> lpush names Wiktor
(integer) 5
```

```bash
127.0.0.1:6379> lrange names 0 -1
1) "Wiktor"
2) "Pawel"
3) "Bartek"
4) "Robert"
5) "Jacek"
```

```bash
127.0.0.1:6379> lpop names
"Wiktor"
```

```bash
127.0.0.1:6379> lpop names
"Pawel"
```

```bash
127.0.0.1:6379> lrange names 0 -1
1) "Zuzia"
2) "Bartek"
3) "Robert"
4) "Jacek"
```

```bash
127.0.0.1:6379> rpop names
"Jacek"
```

##### Redis -> zbiory (nieposortowane)
###### Zadanie
Proszę stworzyć zbiór górskich szczytów. Proszę dodawać i usuwać szczyty ze zbioru. Proszę również wypisać wszystkie szczyty. **Dodatkowo** proszę stworzyć osobny zbiór (z kilkoma szczytami z pierwszego zbioru) i wykonać porównanie zbiorów poleceniem `sdiff mountains nazwa_drugiej_listy`.

```bash
127.0.0.1:6379> sadd mountains Jawornik Plasza Rabia_Skala Okraglik Fereczata Smerek Mala_Rawka
(integer) 7
```

```bash
127.0.0.1:6379> sadd mountains Tarnica
(integer) 1
```

```bash
127.0.0.1:6379> smembers mountains
1) "Rabia_Skala"
2) "Okraglik"
3) "Jawornik"
4) "Mala_Rawka"
5) "Plasza"
6) "Tarnica"
7) "Smerek"
8) "Fereczata"
```

```bash
127.0.0.1:6379> sismember mountains Tarnica
(integer) 1
```

```bash
127.0.0.1:6379> sismember mountains Targowica
(integer) 0
```

```bash
127.0.0.1:6379> srem mountains Plasza Tarnica Smerek
(integer) 3
```

```bash
127.0.0.1:6379> smembers mountains
1) "Rabia_Skala"
2) "Okraglik"
3) "Jawornik"
4) "Mala_Rawka"
5) "Fereczata"
```

**Proszę wykonać polecenie opisane na początku tej sekcji.**

##### Redis -> zbiory (posortowane)
###### Zadanie
Proszę stworzyć kolekcję albumów muzycznych, w której wartością sortującą będzie rok wydania danego albumu. Proszę  wypisać wszystkie albumy. Proszę wypisać wszystkie albumy wraz z rokiem wydania. Proszę wypisać tylko te albumy, które zostały wydane w wybranym przedziale (np. od 1992 roku do 1996 roku).

```bash
127.0.0.1:6379> zadd albums 2000 Bieszczadzkie_anioly 1992 Czarny_blues_o_czwartej_nad_ranem 2010 Blues_nocy_bieszczadzkiej 1990 Dla_wszystkich_starczy_miejsca 2018 Blizny_czasu 2014 Mowi_madrosc 1990 Makatki
(integer) 7
127.0.0.1:6379> zrange albums 0 -1
1) "Dla_wszystkich_starczy_miejsca"
2) "Makatki"
3) "Czarny_blues_o_czwartej_nad_ranem"
4) "Bieszczadzkie_anioly"
5) "Blues_nocy_bieszczadzkiej"
6) "Mowi_madrosc"
7) "Blizny_czasu"
```

```bash
127.0.0.1:6379> zrange albums 0 -1 withscores
 1) "Dla_wszystkich_starczy_miejsca"
 2) "1990"
 3) "Makatki"
 4) "1990"
 5) "Czarny_blues_o_czwartej_nad_ranem"
 6) "1992"
 7) "Bieszczadzkie_anioly"
 8) "2000"
 9) "Blues_nocy_bieszczadzkiej"
10) "2010"
11) "Mowi_madrosc"
12) "2014"
13) "Blizny_czasu"
14) "2018"
```

```bash
127.0.0.1:6379> zadd albums 1992 Live_in_Lowicz
(integer) 1
```

```bash
127.0.0.1:6379> zrangebyscore albums 1992 1996
1) "Czarny_blues_o_czwartej_nad_ranem"
2) "Live_in_Lowicz"
```

```bash
127.0.0.1:6379> zrem albums Mowi_madrosc
(integer) 1
```

##### Redis -> mapa
###### Zadanie
Proszę stworzyć użytkownika (imię, wiek, status konta). Proszę wypisać wszystkie pola danego użytkownika. Proszę wypisać tylko: imię, wiek, status konta. Proszę usunąć pola: "wiek", "status konta".

```bash
127.0.0.1:6379> hset user_1 name Pawel age 18 valid true
(integer) 3
```

```bash
127.0.0.1:6379> hgetall user_1
1) "name"
2) "Pawel"
3) "age"
4) "18"
5) "valid"
6) "true"
```

```bash
127.0.0.1:6379> hget user_1 name
"Pawel"
```

```bash
127.0.0.1:6379> hget user_1 age
"18"
```

```bash
127.0.0.1:6379> hget user_1 valid
"true"
```

```bash
127.0.0.1:6379> hdel user_1 age
(integer) 1
```

```bash
127.0.0.1:6379> hdel user_1 valid
(integer) 1
```

```bash
127.0.0.1:6379> hgetall user_1
1) "name"
2) "Pawel"
```

### Aplikacja, redis, sesja

#### Struktura katalogów
Proszę stworzyć poniżej przedstawione drzewo projektu.
```bash
lab_6
├── docker-compose.yml
├── Dockerfile
└── fourth_app
    ├── __pycache__
    │   └── session_app.cpython-37.pyc
    ├── requirements.txt
    ├── session_app.py
    ├── static
    │   ├── favicon.ico
    │   └── styles
    │       └── style.css
    └── templates
        ├── errors
        │   ├── 403.html
        │   └── 404.html
        ├── index.html
        ├── login.html
        └── secure
            └── secure-page.html
```

##### Dockerfile

Proszę zwrócić uwagę na dodatkowe biblioteki dołączane do tego projektu. Tym razem uruchomimy naszą aplikacją po zaimprowizowanym `SSL-u`.

```bash
FROM python:3.7-alpine
WORKDIR /fourth_app
ENV FLASK_APP session_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80
COPY ./fourth_app /fourth_app
RUN pip install -r requirements.txt
RUN apk add --no-cache gcc musl-dev linux-headers openssl-dev libffi-dev
RUN pip install pyopenssl
CMD ["flask", "run", "--cert", "adhoc"]
```

##### Docker-compose.yml

Dla wygody pracy zostawiamy zmienną środowiskową `FLASK_ENV` z wartością `development`. Dzięki temu będziemy mogli wykonywać zmiany on-line (tzn. bez ciągłego ręcznego budowania aplikacji po wprowadzeniu zmiany). Proszę zwrócić uwagę na mapowanie portów (`8080:80`) -- w przypadku "laboratoryjnego" `RancherOS` konfiguracja wymaga zmiany na `80:80`.

```bash
version: '3'
services:
  web:
    build: .
    ports:
      - "8080:80"
    volumes:
      - ./fourth_app:/fourth_app
    environment:
      FLASK_ENV: development
  redis:
    image: "redis:alpine"
```

##### Requirements (TXT)

```bash
flask
redis
```


##### Login (HTML)

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Redis & Session</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
		<h2>Zażółć gęślją... jaźń!</h2>
        </header>
        <section>
            <form action="https://localhost:8080/login" method="POST">
                <input type="text" name="username" />
                <input type="submit" value="Zaloguj"/>
            </form>
        </section>
    </body>
</html>
```

##### Index (HTML)

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Redis & Session</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <section>
            <p>To jest strona główna: zażółć gęślą jaźń.</p>
            <ul>
                <li><a href="{{ url_for('secret_page')}}">Link do strony zabezpieczonej</a></li>
                <li><a href="{{ url_for('login') }}"> Link do strony logowania</li>
            </ul>
        </section>
    </body>
</html>
```

##### Secure-page (HTML)

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Redis & Session</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <section>
            <p>To jest strona <strong>zabezpieczona.</strong></p>
            <ul>
                <li><a href="{{ url_for('index') }}"> Link do strony głównej</li>
            </ul>
        </section>
    </body>
</html>
```

##### 403 (HTML)

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Redis & Session</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
            <h1>403</h1>
        </header>
        <section>            
            <p>Nie masz dostępu do zasobu.
                <a href="{{ url_for('login') }}"> Zawróć.</a>
            </p>
            <p>
                <strong>Więcej o błędzie:</strong>
                <br/>
                {{error}}
            </p>
        </section>
    </body>
</html>
```

##### 404 (HTML)

```bash
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Redis & Session</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
            <h1>404</h1>
        </header>
        <section>            
            <p>
                Dojechałeś do ślepej uliczki.
                <br/>
                Nie wiemy, gdzie jesteś.
                <br/>
                Nie możemy wysłać wsparcia.
                <a href="{{ url_for('login') }}"> Zawróć.</a>
            </p>
            <p>
                <strong>Więcej o błędzie:</strong>
                <br/>
                {{error}}
            </p>
        </section>
    </body>
</html>
```

##### Style (CSS)

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
}
```

##### Session-app (PY)

```python
from flask import Flask, request, render_template, redirect, url_for, make_response, abort
import redis
import hashlib

POST = "POST"
GET = "GET"
SESSION_ID = "session-id"

app = Flask(__name__, static_url_path = "")
db = redis.Redis(host = "redis", port = 6379, decode_responses = True)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/login", methods = [GET, POST])
def login():
    if(request.method == POST):
        username = request.form["username"].encode("utf-8")
        name_hash = hashlib.sha512(username).hexdigest()
        db.set(SESSION_ID, name_hash)
        response = make_response(render_template("index.html"))
        response.set_cookie(SESSION_ID, name_hash, max_age = 30, secure = True, httponly = True)
        return response

    return render_template("login.html")

@app.route("/secure", methods = [GET])
def secret_page():
    name_hash = request.cookies.get(SESSION_ID)
    if(name_hash != None):
        return render_template("secure/secure-page.html")
    else:
        abort(403)

@app.errorhandler(403)
def page_forbidden(error):
    return render_template("errors/403.html", error = error)

@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html", error = error)
```

#### Zadania
Zostaną ustalone w trakcie zajęć.

## Lab 7 JWT
Żetony JWT (JSON Web Tokens) pozwalają na autoryzację robioną w inny sposób niż `ID sesji`. Żetony potwierdzają, że ktoś już za nas poświadczył naszą wiarygodność, tzn. ktoś nas już zidentyfikował i możemy iść dalej.
Jednak ze względu na znane słabości żetonów, nie należy ich (w takiej podstawowej wersji) używać jako wyłącznego mechanizmu autoryzacji. Żetony (`JWT`) bywają zastępczo wykorzystywane zamiast `ID sesji` jednak często jest to robione w sztuczny sposób (tokeny są ważne przez ustalony z góry czas, więc w przypadku konieczności wylogowania użytkownika (przez administratora) przed czasem ważności tokenu, powstanie potrzeba przechowywania tokenów w bazie danych, co sprawi, że sama idea używania żetonów, jako mechanizmu autoryzacji w bezstanowych aplikacjach, zostanie poważnie podważona).

Celem zajęć jest (niepełne) pokazanie podstawowego działania tokenów jako mechanizmu autoryzacji w aplikacji bezstanowej. W wyniku uruchomienia poniżej przedstawionych kodów źródłowych powinny powstać dwie aplikacje:

 - jwt_app -- aplikacja, która "po zalogowaniu" zwraca token (`T1`);
 - files_app -- aplikacja, która wykorzystując token (`T1`) pozwoli pobrać plik z aplikacji.

Elementem, którego w oczywisty sposób brakuje w przedstawionym przykładzie, jest ID sesji, które powinno być zwracane "po zalogowaniu". Jednak uzupełnienie tego elementu, na podstawie poprzednich zajęć, pozostaje do wykonania jako jedno z zadań laboratoryjnych.

### Struktura katalogów
Należy przygotować poniżej przedstawiona strukturę katalogów. Proszę zwrócić uwagę na odmienny, w porównaniu do poprzednich zajęć, układ plików. Tym razem istnieją dwa pliki `Dockerfile` -- dla `jwt_app` (katalog: `login`) oraz dla `files_app` (katalog: `files`).

```sh
lab_7
├── Docker
│   ├── files
│   │   └── Dockerfile
│   └── login
│       └── Dockerfile
├── docker-compose.yml
└── fifth_app
    ├── files
    │   └── cos.png
    ├── files_app.py
    ├── jwt_app.py
    ├── requirements.txt
    ├── static
    │   ├── favicon.ico
    │   └── styles
    │       └── style.css
    └── templates
        ├── index-files.html
        └── index.html
```

### Dockerfile
Poniżej pokazano zawartość dwóch plików `Dockerfile`. Różnią się między sobą wartościami: `FLASK_APP`, `FLASK_RUN_PORT`.

#### Docker -> login  (`Dockerfile`)

```sh
FROM python:3.7-alpine
WORKDIR /fifth_app
ENV FLASK_APP jwt_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80
COPY ./fifth_app /fifth_app
RUN pip install -r requirements.txt
RUN apk add --no-cache gcc musl-dev linux-headers openssl-dev libffi-dev
RUN pip install pyopenssl
RUN pip install flask-jwt-extended
CMD ["flask", "run", "--cert", "adhoc"]
```

#### Docker -> files (`Dockerfile`)

```sh
FROM python:3.7-alpine
WORKDIR /fifth_app
ENV FLASK_APP files_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 81
COPY ./fifth_app /fifth_app
RUN pip install -r requirements.txt
RUN apk add --no-cache gcc musl-dev linux-headers openssl-dev libffi-dev
RUN pip install pyopenssl
RUN pip install flask-jwt-extended
CMD ["flask", "run", "--cert", "adhoc"]
```

### docker-compose.yml
Zawartość pliku `docker-compose.yml` również wymaga dodatkowego komentarza. Definiowane są dwa obrazy: `web`, `web_files`. Obrazy w swoich konfiguracjach różnią się między sobą mapowaniem portów oraz ścieżką do pliku `Dockerfile`. Warto zwrócić uwagę na atrybut `context`, który sprawia, że polecennia wewnątrz `Dockerfile` będą odwoływać się do katalogu wskazanego przez ten atrybut (`context`).

Kolejnym nowym elementem jest tworzenie zmiennej środowiskowej dla `Docker-a` (`LAB_5_SECRET`). Do tej zmiennej przypisywana jest wartość zmiennej środowiskowej (`LAB_5_SECRET` -- odwołanie ze znakiem `$`) systemu gospodarza (w naszym przypadku: `RancherOS`).

```sh
version: '3'
services:
  web:
    build:
      context: .
      dockerfile: ./Docker/login/Dockerfile
    ports:
      - "80:80"
    volumes:
      - ./fifth_app:/fifth_app
    environment:
      FLASK_ENV: development
      LAB_5_SECRET: $LAB_5_SECRET
  web-files:
    build:
      context: .
      dockerfile: ./Docker/files/Dockerfile
    ports:
      - "81:81"
    volumes:
      - ./fifth_app:/fifth_app
    environment:
      FLASK_ENV: development
      LAB_5_SECRET: $LAB_5_SECRET
  redis:
    image: "redis:alpine"
```

### requirements.txt

```sh
flask
redis
```

### Login (jwt_app.py)

```python
from flask import Flask, request, render_template
import redis
from flask_jwt_extended import JWTManager, create_access_token, jwt_required
import os

GET = "GET"
POST = "POST"
SECRET_KEY = "LAB_5_SECRET"
TOKEN_EXPIRES_IN_SECONDS = 30

app = Flask(__name__, static_url_path = "")
db = redis.Redis(host = "redis", port = 6379, decode_responses = True)

app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)

app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS
jwt = JWTManager(app)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/login", methods = [POST])
def login():
    username = request.form["username"]
    access_token = create_access_token(identity = username)
    return {"access_token" : access_token}

@app.route("/secret", methods = [GET])
@jwt_required
def secret():
    return {"secret-info" : "123456789"}
```

### Files (files_app.py)

```python
from flask import Flask, request, render_template, send_file
import redis
from flask_jwt_extended import JWTManager, create_access_token, jwt_required
import os, sys

GET = "GET"
SECRET_KEY = "LAB_5_SECRET"
TOKEN_EXPIRES_IN_SECONDS = 30

app = Flask(__name__, static_url_path = "")
db = redis.Redis(host = "redis", port = 6379, decode_responses = True)

app.config["JWT_SECRET_KEY"] = os.environ.get(SECRET_KEY)

app.config["JWT_ACCESS_TOKEN_EXPIRES"] = TOKEN_EXPIRES_IN_SECONDS
jwt = JWTManager(app)

@app.route("/")
def index():
    return render_template("index-files.html")

@app.route("/secret-files", methods = [GET])
@jwt_required
def secret():
    return {"secret-info" : "to jest strona z tajnymi dokumentami PDF"}

@app.route("/download-files/", methods = [GET])
@jwt_required
def download_file():
    try:
        return send_file("files/cos.png")
    except Exception as e:
        return print(str(e), file = sys.stderr)
```

### Login (index.html)

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>JWT</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <section>
            <p>JWT: zażółć gęślą jaźń.</p>
        </section>
    </body>
</html>
```

### Files (index-files.html)

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>FILES</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    </head>
    <body>
        <section>
            <p>To jest strona główna dla pobierania 
                <a href="/download-files/" target="blank" download><button>plików</button></a>.
            </p>
        </section>
    </body>
</html>
```

### (style.css)
```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
}
```

### Uruchomienie
Po napisaniu powyżej przedstawionych kodów źródłowych, należy stworzyć zmienną środowiskową w systemie gospodarza (`RancherOS`).

```sh
[rancher@rancher ~]$ export env LAB_5_SECRET=42312314
[rancher@rancher ~]$ echo $LAB_5_SECRET
```

Całość uruchamiana jest poleceniem (będąc w katalogu z plikiem `docker-compose.yml`):

```sh
[rancher@rancher ~]$ docker-compose up --build
```

##### Generowanie losowych wartości
Wartość zmiennej środowiskowej czasami należy wygenerować w bardziej profesjonalny sposób. Wtedy można skorzystać z narzędzia `openssl`.

```sh
user@ubuntu:$ openssl rand -base64 32
```

### Authorization Bearer
W domyślnej konfiguracji żetonów (`JWT` dla `Flask` -> `flask-jwt-extended`), wartości żetonów należy przesyłać w nagłówku żądania. Nagłówek: `Authorization` z wartością `Bearer <wygenerowany_żeton>`.

### Zadania do zrealizowania w ramach zajęć
1. Proszę uruchomić aplikację i potwierdzić jej działanie poprzez otwarcie w przeglądarce: `https://localhost:8080`, `https://localhost:8081`.
2. Proszę wykorzystać `Postman-a` do wysłania żądania `POST` pod `https://localhost:8080/login`. W ciele żądania (`body`) należy podać parametr `username` z jakąś wartością. To ma symbolizować proces logowania (uzupełnienie tego procesu o podawanie hasła będzie elementem drugiego kamienia milowego w realizowanym projekcie). W odpowiedzi powinien przyjść `access_token`. Proszę pamiętać, że ważność tego tokenu (w sekundach) jest zdefiniowana przez stałą `TOKEN_EXPIRES_IN_SECONDS`.
3. Uzyskany `access_token` należy wysłać w żądaniu `GET` pod `https://localhost:8081/secret-files`. Proszę pamiętać, że token należy przesłać w nagłówku (co zostało opisane w [Authorization Bearer](#markdown-header-authorization-bearer))
4. Analogicznie do zadania 3. należy wykonać próbę pobrania pliku żądaniem `GET` pod `https://localhost:8081/download-files`.
5. Proszę przekazywać token (po zalogowaniu) jako wartość, którą można zapisać po stronie klienta (np. w "ciasteczku"). Taką wartość trzeba będzie następnie przesyłać jako nagłówek w kolejnych żądaniach (analogicznie do tego, co było robione przez `Postman-a`).

## Lab 8 Rest
Zajęcia poświęcone są usługom sieciowym REST (Representational state transfer). Na przykładzie zamieszczonego kodu spróbujemy zobrazować, czym jest model dojrzałości Richardsona (Richardson Maturity Problem).

### Struktura katalogów
Struktura plików / katalogów, które trzeba przygotować, została przedstawiona poniżej.

```sh
lab_8
├── docker-compose.yml
├── Dockerfile
└── sixth_app
    ├── requirements.txt
    ├── rest_full_app.py
    └── src
        ├── dto
        │   ├── request
        │   │   ├── author_request.py
        │   │   └── book_request.py
        │   └── response
        │       └── paginated_book_response.py
        ├── exception
        │   └── exception.py
        └── service
            ├── author_service.py
            ├── book_service.py
            ├── entity
            │   ├── author.py
            │   └── book.py
            └── repositories
                ├── author_repository.py
                └── book_repository.py
```

### docker-compose.yml

```sh
version: "3"
services:
    web:
      build: .
      ports:
        - "80:80"
      volumes:
        - ./sixth_app:/sixth_app
      environment:
        FLASK_ENV: development
    redis:
      image: "redis:alpine"
```

### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /sixth_app
ENV FLASK_APP rest_full_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80
COPY ./sixth_app /sixth_app
RUN pip install -r requirements.txt
CMD ["flask", "run"]
```

### requirements.txt
Proszę zwrócić uwagę na dołączenie `flask-restplus`. Dzięki tej bibliotece nasza aplikacja zostanie zbudowana ze `Swagger-em` (popularnym narzędziem wspierającym: budowanie, dokumentowanie oraz testowanie API REST-owego).

```txt
flask
flask-restplus
redis
```

### rest_full_app.py (Python)

W tym istotnym fragmencie całej aplikacji należy zauważyć wykorzystanie biblioteki `flask-restplus`. To właśnie dzięki:

```python
api_app = Api(app = app, version = "...", title = "...", description = "...")
```

można skorzystać z domyślnej konfiguracji `Swagger-a`, którego interfejs webowy zostanie wystawiony pod adresem (`entrypoint`) zgodnym z naszą konfiguracją w plikach `Dockerfile` i `docker-compose.yml` (w przeglądarce klienta (`Ubuntu`) należy odwiedzić: `http://localhost:8080` -- zgodnie z mapowaniem portów przez laboratoryjną maszynę wirtualną `RancherOS`).

W tej przykładowej aplikacji zostały zdefiniowane trzy główne klasy określające grupę `endpoint-ów`: `hello_namespace` (Hello), `author_namespace` (Author), `book_namespace` (Book). Implementacja metod z nazwami odpowiadającymi czasownikom HTTP pozwala na jednoznaczne mapowanie ich przez kod, który automagicznie generuje webowy inerfejs Swagger-a.

Dodatkowo warto zwrócić uwagę na przykładowe wywołanie `api_app.model(...)`:

```python
new_author_model = api_app.model("Author model",
        {
            "name": fields.String(required = True, description = "...", help = "..."),
            "surname": fields.String(required = True, description = "...", help = "...")
        })
```

Dzięki takiej konfiguracji można określić strukturę parametrów oczekiwanych przez konkretny `endpoint`, używając dekoratora:

```python
@api_app.expect(new_author_model)
```

Poniżej jest już widoczny pełny kod źródłowy `rest_full_app.py`.

```python
from flask import Flask, request
from flask_restplus import Api, Resource, fields
from src.dto.request.book_request import BookRequest
from src.dto.request.author_request import AuthorRequest
from src.service.book_service import BookService
from src.service.author_service import AuthorService
from src.exception.exception import BookAlreadyExistsException, AuthorAlreadyExistsException, AuthorNotFoundByIdException

app = Flask(__name__)
api_app = Api(app = app, version = "0.1", title = "Sixth app API", description = "REST-full API for library")

hello_namespace = api_app.namespace("hello", description = "Info API")
author_namespace = api_app.namespace("author", description = "Author API")
book_namespace = api_app.namespace("book", description = "Book API")

START = "start"
LIMIT = "limit"

@hello_namespace.route("/")
class Hello(Resource):
    
    @api_app.doc(responses = {200: "OK, Hello World"})
    def get(self):
        return {
                "message": "Hello, World!"
        }

@author_namespace.route("/")
class Author(Resource):

    def __init__(self, args):
        super().__init__(args)
        self.author_service = AuthorService()

    new_author_model = api_app.model("Author model",
            {
                "name": fields.String(required = True, description = "Author name", help = "Name cannot be blank"),
                "surname": fields.String(required = True, description = "Author surname", help = "Surname cannot be null") 
            })

    @api_app.expect(new_author_model)
    def post(self):
        try:
            author_req = AuthorRequest(request)
            saved_author_id = self.author_service.add_author(author_req)
            
            result = {"message": "Added new author", "save_author_id": saved_author_id}

            return result

        except AuthorAlreadyExistsException as e:
            author_namespace.abort(409, e.__doc__, status = "Could not save author. Already exists", statusCode= 409)


@book_namespace.route("/<int:id>")
class Book(Resource):

    @api_app.doc(responses = {200: "OK", 400: "Invalid argument"},
            params = {"id": "Specify book Id"})
    def get(self, id):
        try:

            return {
                    "message": "Found book by id: {0}".format(id)
            }

        except Exception as e:
            book_namespace.abort(400, e.__doc__, status = "Could not find book by id", statusCode = "400")
    
    @api_app.doc(responses = {200: "OK", 400: "Invalid argument"},
            params = {"id": "Specify book Id to remove"})
    def delete(self, id):
        try:

            return {
                    "message": "Removed book by id: {0}".format(id)
            }

        except Exception as e:
            book_namespace.abort(400, e.__doc__, status = "Could not remove book by id", statusCode = "400")

@book_namespace.route("/list")
class BookList(Resource):

    def __init__(self, args):
        super().__init__(args)
        self.book_service = BookService()
        self.author_service = AuthorService()

    new_book_model = api_app.model("Book model",
            {
                "title": fields.String(required = True, description = "Book title", help = "Title cannot be null", example = "Bieguni"),
                "year": fields.Integer(required = True, description = "Year of publication", help = "Year cannot be null", example = "2007"),
                "author_id": fields.Integer(required = True, description = "Author's Id ", help = "Author's Id cannot be null")
            })

    @api_app.param(START, "The data will be returned from this position.")
    @api_app.param(LIMIT, "The max size of returned data.")
    @api_app.doc(responses = {200: "OK"})
    def get(self):
        start = self.parse_request_arg_or_zero(request, START, "0")
        start = max(1, start)
        limit = self.parse_request_arg_or_zero(request, LIMIT, "50")

        paginated_book_response = self.book_service.get_paginated_books_response(start, limit)
        
        return paginated_book_response.get_json(request.base_url)

    def parse_request_arg_or_zero(self, request, param, default_value):
        val = request.args.get(param, default_value)
        val = int(val) if val.isdigit() else 0
        return val

    @api_app.expect(new_book_model)
    def post(self):
        try:
            book_req = BookRequest(request)
            author = self.author_service.get_author_by_id(book_req.author_id)
            saved_book_id = self.book_service.add_book(book_req)

            result = {"message": "Added new book", "saved_book_id": saved_book_id}

            return result

        except KeyError as e:
            book_namespace.abort(400, e.__doc__, status = "Could not save new book", statusCode = "400")

        except BookAlreadyExistsException as e:
            book_namespace.abort(409, e.__doc__, status = "Could not save new book. Already exists", statusCode = "409")

        except AuthorNotFoundByIdException as e:
            book_namespace.abort(404, e.__doc__, status = "Could not save new book. Author (by id) does not exist.", statusCode = "404")
```

### DTO (Data Transfer Object)
Poniższe trzy kody źródłowe zostały stworzone do obiektowej reprezentacji otrzymywanych / zwracanych danych. Przy użyciu takich obiektów wygodniej jest przekazywać informacje (dane) pomiędzy kolejnymi (zdalnymi) serwisami całej aplikacji. W przypadku prezentowanej aplikacji taka struktura może wydawać się nadmiarowa, jednak ze względów praktycznych została zaimplementowana.

### dto/request/author_request.py (Python)

```python
class AuthorRequest:
    def __init__(self, request):
        self.name = request.json["name"]
        self.surname = request.json["surname"]

    def __str__(self):
        return "name: {0}, surname: {1}".format(self.name, self.surname)
```

### dto/request/book_request.py (Python)

```python
class BookRequest:
    def __init__(self, request):
        self.author_id = request.json["author_id"]
        self.title = request.json["title"]
        self.year = request.json["year"]

    def __str__(self):
        return "author_id: {0}, title: {1}, year: {2}".format(self.author_id, self.title, self.year)
```

### dto/response/paginated_book_response.py (Python)

```python
from flask import jsonify
import json

class PaginatedBookResponse:
    def __init__(self, books, start, limit, count):
        self.books = []

        for book in books:
            self.books.append(book.__dict__)

        self.start = start
        self.limit = limit
        self.current_size = len(books)
        self.count = count

    def get_json(self, url):
        if self.start <= 1:
            previous_url = ""
        else:
            start_previous = max(1, self.start - self.limit)
            previous_url = "{0}?start={1}&limit={2}".format(url, start_previous, self.limit)

        if self.start + self.limit > self.count:
            next_url = ""
        else:
            start_next = self.start + self.limit
            next_url = "{0}?start={1}&limit={2}".format(url, start_next, self.limit)

        return {
                "books": self.books,
                "start": self.start,
                "limit": self.limit,
                "current_size": self.current_size,
                "count": self.count,
                "previous": previous_url,
                "next": next_url
                }
```

### exception/exception.py (Python)
Poniższe klasy mogą sprawiać wrażenie, że nic się w nich nie dzieje. I to prawda! Ale dzięki jednoznacznych nazw klas wyjątków, łatwiej będzie rozróżnić błędy generowane podczas działania aplikacji.

```python
class BookAlreadyExistsException(Exception):
    pass

class AuthorAlreadyExistsException(Exception):
    pass

class AuthorNotFoundByIdException(Exception):
    pass
```

### service/author_service.py
Proszę zwrócić uwagę, że w poniższym kodzie pierwszy raz wykorzystywane są metody do logowania informacji o przebiegu działania aplikacji (`app.logger.debug("...")`).

```python
from flask import Flask
from src.service.repositories.author_repository import AuthorRepository
from src.exception.exception import AuthorNotFoundByIdException

app = Flask(__name__)

class AuthorService:

    def __init__(self):
        self.author_repo = AuthorRepository()

    def add_author(self, author_req):
        app.logger.debug("Adding author...")
        author_id = self.author_repo.save(author_req)
        app.logger.debug("Added author (id: {0})".format(author_id))
        return author_id

    def get_author_by_id(self, author_id):
        app.logger.debug("Getting author by id: {0}.".format(author_id))
        author = self.author_repo.find_by_id(author_id)

        if author == None:
            raise AuthorNotFoundByIdException("Not found author by id: {0}".format(author_id))

        app.logger.debug("Got author by id: {0}".format(author_id))
        return author
```

### book_service.py (Python)

```python
from flask import Flask
from src.service.repositories.book_repository import BookRepository
from src.dto.response.paginated_book_response import PaginatedBookResponse

app = Flask(__name__)

class BookService:

    def __init__(self):
        self.book_repo = BookRepository()

    def add_book(self, book_req):
        app.logger.debug("Adding book...")
        book_id = self.book_repo.save(book_req)
        app.logger.debug("Added book (id: {0})".format(book_id))
        return book_id
    
    def get_paginated_books_response(self, start, limit):
        app.logger.debug("Getting paginated books (start: {0}, limit: {1})".format(start, limit))
        n_of_books = self.book_repo.count_all()
        
        books = self.book_repo.find_n_books(start, limit)

        books_response = PaginatedBookResponse(books, start, limit, n_of_books)

        app.logger.debug("Got paginated books (start: {0}, limit: {1}, count: {2}, current_size: {3})".format(start, limit, n_of_books, len(books)))
        return books_response
```

### @classmethod
W poniżej reprezentowanych dwóch plikach (`book.py`, `author.py`) wykorzystano `pie-decorator`, czyli składnię dekoratorów (z symbolem`@`) znaną z innych języków programwania (np. Javy). W tym przypadku `@classmethod` pełni rolę oznaczenia metody, którą można rozumieć jako metodę statyczną -- do jej wywołania nie jest potrzebna konkretna instancja klasy (obiekt).

### service/entity/book.py (Python)

```python
import json

class Book:
    def __init__(self, id, author_id, title, year):
        self.id = id
        self.author_id = author_id
        self.title = title
        self.year = year

    @classmethod
    def from_json(cls, data):
        return cls(**data)
```

### service/entity/author.py (Python)

```python
import json

class Author:
    def __init__(self, id, name, surname):
        self.id = id
        self.name = name
        self.surname = surname

    @classmethod
    def from_json(cls, data):
        return cls(**data)
```

### service/repositories/author_repository.py (Python)

```python
from flask import Flask
import redis
import json

from ...service.entity.author import Author
from ...exception.exception import AuthorAlreadyExistsException

app = Flask(__name__)

AUTHOR_COUNTER = "author_counter"
AUTHOR_ID_PREFIX = "author_"

class AuthorRepository:

    def __init__(self):
        self.db = redis.Redis(host = "redis", port = 6379, decode_responses = True)
        if self.db.get(AUTHOR_COUNTER) == None:
            self.db.set(AUTHOR_COUNTER, 0)
    
    def save(self, author_req):
        app.logger.debug("Saving new author: {0}.".format(author_req))
        author = self.find_by_names(author_req.name, author_req.surname)

        if author != None:
            raise AuthorAlreadyExistsException("Author (name: \"{0}\", surname: \"{1}\") already exists".format(author_req.name, author_req.surname))

        author = Author(self.db.incr(AUTHOR_COUNTER), author_req.name, author_req.surname)

        author_id = AUTHOR_ID_PREFIX + str(author.id)
        author_json = json.dumps(author.__dict__)

        self.db.set(author_id, author_json)

        app.logger.debug("Saved new author: (id: {0}).".format(author.id))
        return author.id

    def find_by_names(self, name, surname):
        n = int(self.db.get(AUTHOR_COUNTER))

        for i in range(1, n + 1):
            author_id = AUTHOR_ID_PREFIX + str(i)

            if not self.db.exists(author_id):
                continue

            author_json = self.db.get(author_id)
            author = Author.from_json(json.loads(author_json))

            if author.name == name and author.surname == surname:
                return author

        return None

    def find_by_id(self, author_id_to_find):
        n = int(self.db.get(AUTHOR_COUNTER))

        for i in range(1, n + 1):
            author_id = AUTHOR_ID_PREFIX + str(i)

            if not self.db.exists(author_id):
                continue

            author_json = self.db.get(author_id)
            author = Author.from_json(json.loads(author_json))

            if author.id == author_id_to_find:
                return author

        return None
```

### service/repositories/book_repository.py (Python)

```python
from flask import Flask
import redis
import json

from ...service.entity.book import Book
from ...exception.exception import BookAlreadyExistsException

app = Flask(__name__)

BOOK_COUNTER = "book_counter"
BOOK_ID_PREFIX = "book_"

class BookRepository:
    
    def __init__(self):
        self.db = redis.Redis(host = "redis", port = 6379, decode_responses = True)
        if self.db.get(BOOK_COUNTER) == None:
            self.db.set(BOOK_COUNTER, 0)
    
    def save(self, book_req):
        app.logger.debug("Saving new book: {0}.".format(book_req))
        book = self.find_book_by_title(book_req.title)

        if book != None:
            raise BookAlreadyExistsException("Book title \"{0}\" already exist.".format(book_req.title))
            
        book = Book(self.db.incr(BOOK_COUNTER), book_req.author_id, book_req.title, book_req.year)
        
        book_id = BOOK_ID_PREFIX + str(book.id)
        book_json = json.dumps(book.__dict__)
        
        self.db.set(book_id, book_json)
        
        app.logger.debug("Saved new book: (id: {0}).".format(book.id))
        return book.id

    def find_book_by_title(self, title):
        n = int(self.db.get(BOOK_COUNTER))

        for i in range(1, n + 1):
            book_id = BOOK_ID_PREFIX + str(i)
            
            if not self.db.exists(book_id):
                continue
            
            book_json = self.db.get(book_id)
            book = Book.from_json(json.loads(book_json))
            
            if book.title == title:
                return book

        return None

    def count_all(self):
        app.logger.debug("Starting counting all books")
        n = int(self.db.get(BOOK_COUNTER))

        n_of_books = 0

        for i in range(1, n + 1):
            book_id = BOOK_ID_PREFIX + str(i)

            if self.db.exists(book_id):
                n_of_books += 1

        app.logger.debug("Counted all books (n: {0})".format(n_of_books))
        return n_of_books

    def find_n_books(self, start, limit):
        app.logger.debug("Finding n of books (start: {0}, limit: {1}".format(start, limit))
        n = int(self.db.get(BOOK_COUNTER))

        books = []
        counter = 1
        
        for i in range(1, n + 1):
            book_id = BOOK_ID_PREFIX + str(i)

            if not self.db.exists(book_id):
                continue

            if counter < start:
                counter += 1
                continue

            book_json = self.db.get(book_id)
            book = Book.from_json(json.loads(book_json))
            books.append(book)

            if len(books) >= limit:
                break

        app.logger.debug("Found {0} books.".format(len(books)))
        return books
```

### Ważne
Przedstawione powyżej fragmenty kodu źródłowego, ze wskazaniem na metody do paginacji tabeli z książkami, charakteryzują (niepełną) usługę sieciową REST na trzecim poziomie dojrzałości Richardsona. Leonard Richardson jest twórcą modelu, w którym rozróżnia się cztery poziomy.

1. (Level 0) Jeżeli nasz serwis udostępnia tylko jeden endpoint, który służy za punkt wejścia i wyjścia do wszelkiej komunikacji -- czyli pełni rolę semantycznie zbliżoną do RPC (remote procedure call) -- to można powiedzieć, że jest to najniższy poziom w modelu dojrzałości Richardsona. W ten sposób mogą działać usługi sieciowe typu SOAP. Najczęściej dane wysyłane są jako XML, a wiadomość zwrotna (zależna od stanu zasobów) również odsyłana jest w tej formie. Kolejne zapytanie (ze zmienioną strukturą XML-a) wysyłane jest pod ten sam `endpoint`, a wiadomość zwrotna (z prawdopodobnie inną strukturą XML w porównaniu do poprzedniej) również wysyłana jest jako XML.
2. (Level 1) Na tym poziomie dojrzałości serwis udostępnia kilka `entry-point-ów` / `endpoint-ów` w zależności od zasobów, po które chcemy sięgnąć. Odnosząc się do prezentowanego powyżej przykładu kodu źródłowego, można powiedzieć, że jeżeli będziemy mieli osobne adresy (URI) do zapytań związanych z książkami i osobne adresy (URI) do zapytań związanych z autorami książek, a wszystkie zapytania będą odbywały się bez rozróżnienia czasowników HTTP (czyli będziemy używali jednego typu żądań, np. POST), to serwis będzie na pierwszym poziomie w modelu dojrzałości Richardsona.
3. (Level 2) Kolejny poziom odróżnia się od poprzedniego poprzez czytelność wykonywanych operacji. Jeżeli komunikacja odbywa się przez HTTP, to należy wykorzystywać czasowniki HTTP (GET, POST, PUT, DELETE, ...) dla rozróżnienia typu operacji podczas odwoływania się po konkretny zasób (pod konkretne URI). Oprócz czasowników HTTP należy również wykorzystywać kody odpowiedzi do sygnalizowania statusu żądania (jeżeli operacja powiodła się, to kod `2xx` (np. `200`) jest oczekiwany, a jeżeli coś się nie udało, to należy to zgłosić wykorzystując jeden z wielu dostępnych kodów odpowiedzi: `4xx`, `5xx`).
4. (Level 3) Poziom często określany jako REST-full API a kojarzony z HATEOAS (Hypertext As The Engine Of Application State). Oznacza to, że jeżeli sięgniemy po jakiś zasób, to w informacji zwrotnej dostaniemy również wiadomość, co możemy dalej z tym zasobem zrobić. W przypadku kodu związanego z paginacją książek w odpowiedzi wysyłane były informacje pod jakim adresem znajdują się (jeśli istnieją): poprzednia strona i następna strona z listą książek. Od serwisu z trzecim poziomem dojrzałości Richardsona można oczekiwać dobrze udokumentowanego, czytelnego API z jednoznacznym opisem używanych kodów odpowiedzi.

### Zadania do zrealizowania w ramach zajęć
1. Proszę uruchomić załączone fragmenty kodu (`docker-compose up`).
2. Proszę sprawdzić wszystkie początkowo istniejące end-pointy za pomocą `Postman-a`.
3. Proszę rozszerzyć / uzupełnić kod w taki sposób, aby możliwe było: dodawanie autora, dodawanie książki (powiązana z autorem), usuwanie książki (po `ID` książki), pobieranie listy wszystkich książek.
4. Proszę odpowiedzieć na pytanie: na którym poziomie modelu dojrzałości Richardsona (RMM) znajduje się opisane rozwiązanie?

## Lab 9 Responsywne
Celem kolejnych zajęć jest przedstawienie podejścia do projektowania aplikacji responsywnych. Przy pierwszym podejściu (dzisiejsze zajęcia) poznamy jedną z najpopularniejszych bibliotek wspierających twórców aplikacji responsywnych.


### Struktura katalogów
```sh
lab_9
├── docker-compose.yml
├── Dockerfile
└── static_html
    └── index.html
```

### Dockerfile
```sh
FROM nginx
COPY ./static_html /usr/share/nginx/html
```

### docker-compose.yml
```sh
version: "3"
services:
    static_web:
       build: .
       ports:
         - "8080:80"
```

### Zadania do wykonania w ramach zajęć
#### Zadanie 1
Proszę przygotować strukturę strony index.html zgodną z poniższym przykładem.

```html
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <title>Seventh app -- bib maker</title>
    </head>
    <body>
        <div>
            <h1>Bib Maker</h1>

            <div>
                <div>
                    <button type="button" class="btn">Niebieski</button>
                    <button type="button" class="btn">Zielony</button>
                    <button type="button" class="btn">Czerwony</button>
                    <button type="button" class="btn">Żółty</button>
                    <button type="button" class="btn">Morski</button>
                </div>

                <hr/>

                <div>
                    <button type="button" class="btn">Niebieski</button>
                    <button type="button" class="btn">Zielony</button>
                    <button type="button" class="btn">Czerwony</button>
                    <button type="button" class="btn">Żółty</button>
                    <button type="button" class="btn">Morski</button>
                </div>

                <hr/>

                <div>
                    <button type="button" class="btn">Niebieski</button>
                    <button type="button" class="btn">Zielony</button>
                    <button type="button" class="btn">Czerwony</button>
                    <button type="button" class="btn">Żółty</button>
                    <button type="button" class="btn">Morski</button>
                </div>
            </div>

            <div>
                <section>
                </section>

                <section>
                </section>

                <section>
                </section>
            </div>

            <hr/>

            <div>
                <section>
                </section>

                <section>
                </section>
            </div>
        </div>

    </body>
</html>
```

#### Zadanie 2
Proszę wypełnić wszystkie sekcje akapitami pozyskanymi z generatora Lorem ipsum.

#### Zadanie 3
Proszę uzupełnić kod o klasy `css` zgodnie z poniższymi punktami.

a.) Pierwszy `div` ma mieć przypisaną klasę `container`.

b.) Wszystkie `div-y` (oprócz pierwszego i drugiego) mają mieć klasę `row`.

c.) Wszystkim znacznikom section należy przypisać klasę `col-sm`. Proszę zaobserwować zmianę układu treści podczas zmiany rozmiaru okna przeglądarki.

#### Zadanie 4
Pierwsze trzy `div-y` z klasą `row` należy wypełnić kolorowymi przyciskami. Kolory mają być wynikiem przypisania klas: `btn-info`, `btn-primary`, `btn-success`, `btn-warning`, `btn-danger`.
Kolory mają odpowiadać treściom (nazwom) przycisków.

#### Zadanie 5
Proszę do wszystkich przycisków z drugiego zestawu przypisać klasę `col-sm`.

#### Zadanie 6
Proszę do przycisków z trzeciego zestawu przypisać kolejno klasy:

- `col-md-4 col-sm-6 col-6`;
- `col-md-4 col-sm-6 col-6`;
- `col-md-4 col-md-2 col-6`;
- `col-md-4 col-sm-6 col-6`;
- `col-sm-8 col-12`.

#### Zadanie 7
Proszę ustalić, jaka jest zależność pomiędzy klasami: `col-4`, `col-sm-4`, `col-md-2`, `col-lg-6`, `col-xl-5`.

#### Zadanie 8
Proszę na stronie umieścić pięć komponentów `card` (z Bootstrap-a). Trzy pierwsze mają być umieszczone w jednym wierszu (klasa `row`). Dwa pozostałe powinny być umieszczone w osobnym wierszu.

Trzy pierwsze komponenty mają zajmować część dostępnej szerokości strony (przy maksymalnej szerokości okna przeglądarki), a dwa kolejne -- całą dostępną szerokość.

Przy minimalnej szerokości okna przeglądarki, wszystkie komponenty powinny być wyświetlone kolejno pod sobą (każdy pojedynczy komponent powinien zająć całą dostępną szerokość).

## Lab 10 Progresywne

W planach: progresywne aplikacje webowe… raczej w domu.

### Zadania do wykonania w ramach zajęć
#### Część pierwsza

1. Zapoznanie się z narzędziem `Lighthouse`. (`Google Chrome` → `Inspect` → `Audits`).
2. Za pomocą narzędzia Lighthouse proszę przeanalizować pięć adresów internetowych. Proszę wskazać, który z tych adresów wypada najlepiej w zestawieniu. Proszę przeanalizować raport Lighthouse dla adresów: najlepszego, najgorszego.
3. Proszę przeanalizować (za pomocą `Lighthouse`) przykład aplikacji progresywnej (od Google) udostępniony w ramach zajęć w serwisie `Glitch.com` (`https://automatic-amaryllis.glitch.me/`).
4. Proszę sprawdzić, jak zachowuje się ta aplikacja, gdy zostanie odłączona od Internetu (`Google Chrome` → `Inspect` → `Application` → `Service Workers` LUB `Google Chrome` → `Inspect` → `Network` → `Offline`).
5. Proszę sprawdzić (w ramach możliwości) działanie aplikacji na urządzeniu mobilnym.

#### Część druga
1. Proszę utworzyć nowy projekt w `Glitch.com`. Wymagana struktura: `/scripts/script.js`, `/styles/style.css`, `index.html`.
2. Strona powinna prezentować tabelę z użytkownikami (kolumny: `Lp.`, `Id`, `Login`, `Imię`, `E-mail`, `Telefon`).
3. Dane użytkowników (`https://jsonplaceholder.typicode.com/users`) powinny być pobierane instrukcją `fetch()` i ładowane do tabeli.
4. Proszę uzupełnić stronę o formularz dodawania użytkowników do tabeli.
5. Strona powinna dobrze prezentować się na trzech rozmiarach urządzeń (smartphone, tablet, monitor komputera). Proszę wykorzystać Bootstrap-a.

#### Część trzecia
1. Proszę zmienić adres do pobierania danych na `https://gorest.co.in/` → `/public-api/users`.

## Lab 11 Gniazda
Gniazda sieciowe są odpowiedzią na potrzeby współczesnego świata w obszarze komunikacji klient-serwer. Zapewniają (przynajmniej w teorii) pełną transmisję dwukierunkową (podobnie jak sieć Ethernet).

Poniżej zebrane są kody źródłowe, które mogą posłużyć jako pomoc przy wykonaniu zadań przewidzianych w ramach zajęć laboratoryjnych.

### Struktura katalogów
Wymagana struktura katalogów, dla zachowania spójności z prezentowanym przykładem kodu, została przedstawiona poniżej.

```txt
lab_11
├── docker-compose.yml
├── Dockerfile
└── eighth_app
    ├── requirements.txt
    ├── static
    │   ├── scripts
    │   │   └── script.js
    │   └── styles
    │       └── style.css
    ├── templates
    │   └── index.html
    └── web_socket_app.py
```

### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /eighth_app
COPY ./eighth_app /eighth_app
RUN apk add --no-cache gcc musl-dev linux-headers
RUN pip install -r requirements.txt
CMD ["python", "web_socket_app.py"]
```

### docker-compose.yml

```sh
version: "3"
services:
  web:
    build: .
    ports:
      - "8080:80"
    volumes:
      - ./eighth_app:/eighth_app
```

### requirements.txt

```txt
flask
flask-socketio
eventlet
```

### index.html

```html
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Web sockets</title>
        <meta charset="utf-8">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/socket.io/1.3.6/socket.io.min.js"></script>
        <script src="/scripts/script.js"></script>
    </head>
    <body>
        <section>
            <p>To jest strona do prezentacji gniazd sieciowych.</p>
        </section>
    </body>
</html>
```

### styles.css

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
}
```


### script.js
Proszę uważnie przeanalizować bałagan przedstawiony poniżej.

```js
document.addEventListener("DOMContentLoaded", function (event) {

    var ws_uri = "http://" + document.domain + ":" + location.port;

    var socket = io.connect(ws_uri);
    
    var SECONDS_2 = 2000;

    socket.on("connect", function () {
        console.log("Correctly connected websocket");
    });

    socket.on("joined_room", function (message) {
        console.log(message);
    });

    socket.emit("message", {data: "Chyba dziala"});

    useragent = navigator.userAgent;
    room_id = "foo_id";
    socket.emit("join", {useragent: useragent, room_id: room_id});

    socket.on("pong", function (message) {
        let ping_value = message.ping_value + 1;
        console.log(ping_value);
        
        setTimeout(function () {
            socket.emit("ping", {ping_value: ping_value});
        }, SECONDS_2);
    });

    var init_value = 1;
    socket.emit("ping", {ping_value: init_value});
});
```

### web_socket_app.py

```python
from flask import Flask, render_template
from flask_socketio import SocketIO, join_room, leave_room, emit, send

app = Flask(__name__, static_url_path = "")
socket_io = SocketIO(app)

USER_AGENT = "useragent"
ROOM_ID = "room_id"
PING_VAL = "ping_value"

@app.route("/")
def index():
    return render_template("/index.html")

@socket_io.on("connect")
def handle_on_connect():
    app.logger.debug("Connected -> OK")
    emit("connection response", {"data": "Correctly connected"})

def handle_on_disconnect():
    app.logger.debug("Disconnected -> Bye")

@socket_io.on("message")
def handle_message(message):
    app.logger.debug("received message: " + str(message))

@socket_io.on("join")
def handle_on_join(data):
    useragent = data[USER_AGENT]
    room_id = data[ROOM_ID]
    join_room(room_id)
    emit("joined_room", {"room_id": room_id})
    app.logger.debug("Useragent: %s added to the room: %s" % (useragent, room_id))

@socket_io.on("leave")
def handle_on_leave(data):
    useragent = data[USER_AGENT]
    room_id = data[ROOM_ID]
    leave_room(room_id)
    app.logger.debug("Useragent: %s is no longer in the room: %s" % (useragent, room_id))

@socket_io.on("ping")
def handle_on_ping(data):
    ping_val = data[PING_VAL]
    ping_val += 1
    max_val = 20
    if(ping_val < max_val):
        emit("pong", {PING_VAL: ping_val})
        app.logger.debug("Pinging -> %s" % (ping_val))
    else:
        app.logger.debug("Ping stop -> %s" % (ping_val))

if __name__ == '__main__':
    socket_io.run(app, host = "0.0.0.0", port = 80, debug = True)
```

### Zadania do wykonania w ramach zajęć

1. Wykorzystując wiedzę z poprzednich (wszystkich) zajęć, proszę zmodyfikować przedstawiony przykład w taki sposób, aby wartości w komunikacji `ping-pong` były wyświetlane (w czasie ich odebrania) w przeglądarce jako lista wypunktowana.
2. Wykorzystując wcześniejsze doświadczenie, proszę stworzyć prosty czat. Okno przeglądarki powinno być podzielone na dwie części (lewą, prawą), które będą symulować okna dwóch użytkowników czatu. Rozmowa powinna odbywać się pomiędzy tymi dwoma okienkami („użytkownikami”) z wykorzystaniem gniazd sieciowych (z biblioteką `SocketIO`).



## Lab 12 Autoryzacja

Kolejne zajęcia są poświęcone mechanizmom zewnętrznej autoryzacji, które (oprócz autoryzacji) pomagają w udostępnianiu innym systemom / aplikacjom / usługom / serwisom danych wymagających poświadczenia przed otrzymaniem do nich dostępu. Najpopularniejszym quasi-standardem (quasi-protokołem) takiej komunikacji stał się `OAuth 2.0`. Popularność i mnogość zastosowań tego fragmentu współczesnej technologii jest tak duża, że trudno dzisiaj byłoby wskazać poważną organizację świadczącą usługi teleinformatyczne, która nie korzystałaby z rozwiązań kryjących się pod enigmatyczną osłoną `OAuth 2.0`. W skrócie chodzi o to, aby zezwolić jednej usłudze (klientowi) na dostęp do innej usługi (do danych).
Z punktu widzenia użytkownika końcowego takie podejście jest wygodne, bo m.in. to właśnie dzięki temu nie trzeba w każdym serwisie / portalu zakładać nowego konta (tworzyć nowego loginu, hasła), choć trzeba potwierdzić znajomość z regulaminem, ponieważ można skorzystać z istniejącego już systemu, który w naszym imieniu poświadczy, że dane autoryzacyjne (a często też identyfikacyjne), których używamy, są prawdziwe.

### Cel zajęć

W trakcie zajęć zostanie postawionych kilka zadań, których wykonanie nie sprawi, że `OAuth` zostanie całkowicie poskromiony i przestanie skrywać tajemnice, ale istnieje duża szansa, że po wykonaniu tych zadań, quasi protokół `OAuth 2.0` przestanie być tak bardzo tajemniczy, jak może sprawiać wrażenie na początku.

### Auth0.com

Do efektywnej pracy podczas tych zajęć zostanie wykorzystany serwis `auth0.com`, w którym -- dla poprawnego przebiegu zadań -- należy założyć konto (i uwaga -- można skorzystać z innych systemów poświadczających nasze uprawnienia). Po udanej rejestracji konta w `auth0.com` należy stworzyć nową aplikację. W tym celu należy postępować zgodnie z poniższymi krokami.

1. Z lewego menu trzeba wybrać `APIs`.
2. W widoku z nagłówkiem `APIs` należy kliknąć na `Create API`.
3. Uzupełnienie pól `Name` i `Identifier` (pozostawiamy algorytm `RS256`) jest proponowanym działaniem zbliżającym nas do kolejnego punktu. Nie ma większego znaczenia, jakimi wartościami zostaną uzupełnione dwa pierwsze pola.
4. Z lewego menu trzeba wybrać `Applications`.
5. W widoku z nagłówkiem `Applications` należy kliknąć na `Create Application`.
6. W oknie tworzenia aplikacji należy uzupełnić pole `Name` oraz wybrać (proponowany typ) `Machine to Machine Applications`.
7. Po kliknięciu na przycisk potwierdzający nasz wybór, należy z listy rozwijanej wybrać API stworzone w poprzednich krokach.
8. Po przejściu do zakładki `Settings` (w ramach tej konkretnej aplikacji) należy uzupełnić pola w następujący sposób. Do `Allowed Callback URLs` należy wpisać `http://localhost:8080/callback`. Do pola `Allowed Logout URLs` należy wpisać `http://localhost:8080/logout_info`. Zmiany należy zachować poprzez kliknięcie na `Save changes`. Nad tym dużym przyciskiem przyciskiem (`Save changes`) jest znacznie mniejszy (`Show Advanced Settings`) -- po kliknięciu należy przejść do zakładki `Grant Types` i wybrać: `Authorization Code` oraz `Refresh Token` (opcjonalnie -- nie będziemy z tego teraz korzystać). **Ważne:** `Client Credentials` powinien pozostać zaznaczony.
9. Jeżeli do tej pory wszystko zostało wykonane poprawnie, to z menu bocznego można wybrać `APIs` a następnie wskazać dodane `API`. Przechodząc do zakładki `Test` można sprawdzić, że wywołanie curl rzeczywiście zwrócić oczekiwany `access_token`.
10. Istotne jest aby z zakładki `Settings` dla wskazanej aplikacji (z okna `Applications`) zanotować `Domain`, `Client ID`, `Client Secret`.
11. Kolejne kroki są już związane z implementacją rozwiązania -- kod jest umieszczony poniżej.

### Zmienne środowiskowe

Dla poprawnej (i bezpiecznej pracy), należy stworzyć dwie zmienne środowiskowe (uzupełniając je odpowiednimi wartościami).

```sh
ubuntu:~$ openssl rand -base64 23
>>> HydJbZrc/7ZM3Z6ETDViMPwIwsBGMwCPWz=
ubuntu:~$ export env LAB_12_SECRET=HydJbZrc/7ZM3Z6ETDViMPw

export env OAUTH_CLIENT_SECRET=UWAGA!!!_Tutaj_należy_wpisać_wartość_CLIENT_SECRET_zanotowaną_z_auth.com
```

### Drzewo plików i katalogów

Należy stworzyć strukturę odpowiadającą tej poniżej przedstawionej.

```sh
lab_12
├── docker-compose.yml
├── Dockerfile
└── ninth_app
    ├── const_config.py
    ├── oauth_client_app.py
    ├── requirements.txt
    ├── static
    │   └── styles
    │       └── style.css
    └── templates
        ├── index.html
        └── secure
            ├── logout.html
            └── secure_page.html
```

### docker-compose.yml

Proszę zwrócić uwagę na mapowanie zmiennych środowiskowych: `LAB_12_SECRET`, `OAUTH_CLIENT_SECRET`.

```sh
version: "3"
services:
   web:
     build: .
     ports:
       - "8080:80"
     volumes:
       - ./ninth_app:/ninth_app
     environment:
       FLASK_ENV: development
       LAB_12_SECRET: $LAB_12_SECRET
       OAUTH_CLIENT_SECRET: $OAUTH_CLIENT_SECRET
```

### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /ninth_app
ENV FLASK_APP oauth_client_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80
COPY ./ninth_app /ninth_app
RUN apk add --no-cache gcc musl-dev linux-headers openssl-dev libffi-dev
RUN pip install -r requirements.txt
CMD ["flask", "run"]
```

### requirements.txt

```txt
flask
requests
authlib
```

### const_config.py

Ten plik należy uzupełnić indywidualnymi wartościami z `auth0.com`.
Pod `OAUTH_BASE_URL` należy wpisać wartość `Domain`, a pod `OAUTH_CLIENT_ID` należy wstawić `Client ID`.

Przykładowy plik został przedstawiony poniżej.

```python
import os

OAUTH_BASE_URL = "https://zawadzp4-pamiw.eu.auth0.com"
OAUTH_ACCESS_TOKEN_URL = OAUTH_BASE_URL + "/oauth/token"
OAUTH_AUTHORIZE_URL = OAUTH_BASE_URL + "/authorize"
OAUTH_CALLBACK_URL = "http://localhost:8080/callback"
OAUTH_CLIENT_ID = "2NnVEhkdwyyM2rQx6RjpTlkiEEKwra3X"
OAUTH_CLIENT_SECRET = os.environ.get("OAUTH_CLIENT_SECRET")
OAUTH_SCOPE = "openid profile"
SECRET_KEY = os.environ.get("LAB_12_SECRET")
NICKNAME = "nickname"
```

### oauth\_client\_app.py

Należy przeanalizować poniższy kod. Proszę zwrócić uwagę na import wszystkich wartości z `const_config.py`. Należy również przeanalizować działanie `authorization_required` -- funkcja wykorzystuje mechanizm dekorowania kodu, czyli nim wykona docelowy kod, to najpierw dokona sprawdzenia warunku (`if NICKNAME not in session:`).
Warto również zwrócić uwagę na fakt, że ten przykład korzysta z innego mechanizmu zarządzania sesją. (W ramach bieżących zajęć można z niego skorzystać. Ale nie jest on dozwolony w trakcie wykonywania kolejnych kamieni milowych projektu zaliczeniowego.)
Z punktu widzenia `OAuth` kluczowa jest funkcja `oauth_callback`. Kiedy (w ramach mechanizmu `OAuth`) zostanie ona wywołana? Proszę odpowiedzieć na to pytanie.

```python
from flask import Flask, render_template, session, url_for, redirect
from authlib.flask.client import OAuth
from functools import wraps
from const_config import *

app = Flask(__name__, static_url_path = "")
app.secret_key = SECRET_KEY
oauth = OAuth(app)

auth0 = oauth.register(
        "lab-12-auth0",
        api_base_url = OAUTH_BASE_URL,
        client_id = OAUTH_CLIENT_ID,
        client_secret = OAUTH_CLIENT_SECRET,
        access_token_url = OAUTH_ACCESS_TOKEN_URL,
        authorize_url = OAUTH_AUTHORIZE_URL,
        client_kwargs = {"scope": OAUTH_SCOPE} )

def authorization_required(f):
    @wraps(f)
    def authorization_decorator(*args, **kwds):
        if NICKNAME not in session:
            return redirect("/login")

        return f(*args, **kwds)

    return authorization_decorator

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/login")
def login():
    return auth0.authorize_redirect(
            redirect_uri = OAUTH_CALLBACK_URL, 
            audience = "")

@app.route("/logout_info")
def logout_info():
    return render_template("./secure/logout.html")

@app.route("/logout")
def logout():
    url_params = "returnTo=" + url_for("logout_info", _external = True)
    url_params += "&"
    url_params += "client_id=" + OAUTH_CLIENT_ID
    
    session.clear()
    return redirect(auth0.api_base_url + "/v2/logout?" + url_params)

@app.route("/secure")
@authorization_required
def secure():
    return render_template("./secure/secure_page.html")

@app.route("/callback")
def oauth_callback():
    auth0.authorize_access_token()
    resp = auth0.get("userinfo")
    nickname = resp.json()["nickname"]

    session[NICKNAME] = nickname

    return redirect("/secure")
```

### style.css

```css
body {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    font-family: 'Cambay', sans-serif;
}
```

### index.html

```html
<!Doctype html>
<html lang="pl">
    <head>
        <title>Ninth App -- OAuth (2.0) example</title>
        <meta charset="utf-8">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        TO JEST STRONA LOGOWANIA
    </body>
</html>
```

### secure_page.html

```html
<!Doctype html>
<html lang="pl">
    <head>
        <title>Ninth App -- OAuth (2.0) example -- Secure page</title>
        <meta charset="utf-8">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        To jest strona, do której dostęp mają tylko użytkownicy zalogowani.
    </body>
</html>
```

### logout.html

```html
<!Doctype html>
<html lang="pl">
    <head>
        <title>Ninth App -- OAuth (2.0) example -- Logout</title>
        <meta charset="utf-8">
        <link href="http://fonts.googleapis.com/css?family=Cambay&subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <link href="/styles/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        Nastąpiło bezpieczne wylogowanie.
    </body>
</html>
```

### Uruchomienie

Po stworzeniu wszystkich plików, całość powinno się zbudować i uruchomić w przeglądarce (`localhost:8080`).

```sh
ubuntu:~$ docker-compose up --build
```

Strony nie mają przycisków nawigacyjnych, ale modyfikując bezpośrednio wartość w pasku adresu, proszę wykonać trzy podstawowe operacje. Proszę się zalogować (`http://localhost:8080/login`). Proszę potwierdzić, że nastąpiło przekierowanie na `http://localhost:8080/secure`. Proszę sprawdzić, że przy próbie przejścia (ponownego wpisania) na `http://localhost:8080/login` następuje automatyczne przekierowanie na stronę `secure`. Proszę się wylogować (w pasku adresu należy wpisać: `localhost:8080/logout`) i potwierdzić, że nastąpiło przekierowanie na stronę `localhost:8080/logout_info`.

Powyższy przykład pokazuje popraną (acz niepełną) próbę wykorzystania quasi-protokołu `OAuth 2.0`.

### Zadania do wykonania w ramach zajęć
1. Proszę zweryfikować logi aplikacji w serwisie `auth0.com` (zakładka `Logs`).
2. Proszę zmodyfikować w `auth0.com` wartości przypisane pod `Grant Types` (proszę odznaczyć `Client Credentials`), a następnie proszę przejść do okna testowania API. Co się stało? Dlaczego? Proszę odpowiedzieć na te "ogólne" pytania.
3. Proszę za pomocą logger-a (`app.logger.debug()`) sprawdzić, jakie informacje są przekazywane w `userinfo`. Proszę zmodyfikować wartość `OAUTH_SCOPE` o dodatkową wartość `email`, a następnie proszę sprawdzić, czy dane przekazywane w `userinfo` uległy zmianie.
4. Proszę odszukać miejsce (w `auth0.com`), w którym są zdefiniowane wszystkie endpoint-y udostępnione w ramach `OAuth`. Jest to przydatna informacja, z której m.in. korzysta się podczas ustalania adresu pobierania żetonów autoryzacyjnych.

## Lab 13 Kolejka

### Struktura katalogów

```sh
lab_13
├── docker-compose.yml
├── Dockerfile
├── README
└── tenth_app
    ├── rabbit_mq_app.py
    └── requirements.txt
```

### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /tenth_app
ENV FLASK_APP rabbit_mq_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80
COPY ./tenth_app /tenth_app
RUN apk add --no-cache gcc musl-dev linux-headers
RUN pip install -r requirements.txt
CMD ["flask", "run"]
```

### docker-compose.yml

```sh
version: "3"
services:
   web:
      build: .
      ports:
        - "8080:80"
      volumes:
        - ./tenth_app:/tenth_app
      environment:
        FLASK_ENV: development
   rabbitmq:
      hostname: lab13_mq
      image: rabbitmq:latest
      ports:
        - "5672:5672"
```

### requirements.txt

```sh
flask
pika
```

### rabbit\_mq\_app.py

```python
from flask import Flask, request
import pika
import time

GET = "GET"
POST = "POST"
QUEUE_NAME = "msg_queue"
DELIVERY_MODE_PERSISTENT = 2
DELAY = 2
NUM_OF_TASK_PER_CONSUMER = 1

app = Flask(__name__, static_url_path = "")

def open_channel():
    rabbit_mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
    channel = rabbit_mq_connection.channel()
    channel.queue_declare(queue = QUEUE_NAME, durable = True)
    return channel

@app.route("/")
def index():
    return "Hello, World!", 200

@app.route("/add-message", methods = [POST])
def add_message_on_queue():
    message = request.form.get("msg")
    
    channel = open_channel()
    channel.basic_publish(
            exchange = "", 
            routing_key = QUEUE_NAME, 
            body = message, 
            properties = pika.BasicProperties(delivery_mode = DELIVERY_MODE_PERSISTENT))
    
    app.logger.debug(message)
    return "Added message", 201

def callback_mq(channel, method, properties, body):
    app.logger.debug("Inside callback_mq()")
    time.sleep(DELAY)
    app.logger.info("Message from queue %s" % body)
    app.logger.info("DONE")
    channel.basic_ack(delivery_tag = method.delivery_tag)


@app.route("/start-consuming-messages", methods = [GET])
def consume_messages():
    channel = open_channel()
    channel.basic_qos(prefetch_count = NUM_OF_TASK_PER_CONSUMER)
    channel.basic_consume(queue = QUEUE_NAME, on_message_callback = callback_mq)
    channel.start_consuming()
```

## Lab 14 Kolejka Jeszcze Raz

### Struktura katalogów

```sh
lab_14
├── docker-compose.yml
├── Dockerfile
├── eleventh_app
│   ├── files_on_rabbit_mq_app.py
│   ├── PDF_files
│   │   ├── in_queue
│   │   └── processed
│   └── requirements.txt
```

### docker-compose.yml

```sh
version: "3"
services:
   web:
      build: .
      ports:
        - "8080:80"
      volumes:
        - ./eleventh_app:/eleventh_app
      environment:
        FLASK_ENV: development
   rabbitmq:
      hostname: lab14_mq
      image: rabbitmq:latest
      ports:
        - "5672:5672"
```

### Dockerfile

```sh
FROM python:3.7-alpine
WORKDIR /eleventh_app
ENV FLASK_APP files_on_rabbit_mq_app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 80
COPY ./eleventh_app /eleventh_app
RUN apk add --no-cache gcc musl-dev linux-headers
RUN pip install -r requirements.txt
CMD ["flask", "run"]
```

### requirements.txt

```sh
flask
pika
PyPDF2
```

### python

```python
from flask import Flask, request
import pika
from threading import Thread
import os
from PyPDF2 import PdfFileReader

PDFS_DIR_PATH = "PDF_files/"
PDFS_DIR_IN_QUEUE = PDFS_DIR_PATH + "in_queue/"
PDFS_DIR_PROCESSED = PDFS_DIR_PATH + "processed/"

GET = "GET"
POST = "POST"
CODING = "utf-8"

QUEUE_NAME = "pdfs_to_process_queue"
DELIVERY_MODE_PERSISTENT = 2
NUM_OF_TASK_PER_CONSUMER = 1

app = Flask(__name__, static_url_path = "")

def open_channel():
    rabbit_mq_connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
    channel = rabbit_mq_connection.channel()
    channel.queue_declare(queue = QUEUE_NAME, durable = True)
    return channel

@app.route("/")
def index():
    return "Hello, World!", 200

@app.route("/upload-pdf", methods = [POST])
def add_pdf_on_queue():
    f = request.files["pdf_file"]

    if(len(f.filename) > 0):

        save_file(f)
    
        channel = open_channel()
        channel.basic_publish(
                exchange = "", 
                routing_key = QUEUE_NAME, 
                body = f.filename, 
                properties = pika.BasicProperties(delivery_mode = DELIVERY_MODE_PERSISTENT))
    
        app.logger.debug(f.filename)
        return "Uploaded file", 201
    else:
        return "Empty uploaded file", 400

def save_file(file_to_save):
    path_to_file = PDFS_DIR_IN_QUEUE + file_to_save.filename
    file_to_save.save(path_to_file)

def callback_mq(channel, method, properties, body):
    app.logger.debug("Filename from queue %s" % body)
    pdf_title = extract_data_from_pdf(body)
    app.logger.debug(pdf_title)
    move_file_to_processed_dir(body)
    app.logger.info("[DONE] File in queue has been processed (%s)" % body)
    channel.basic_ack(delivery_tag = method.delivery_tag)

def extract_data_from_pdf(pdf_filename):
    pdf_title = ""
    try:
        src = PDFS_DIR_IN_QUEUE + str(pdf_filename.decode(CODING))
        pdf = PdfFileReader(open(src, 'rb'))
        pdf_info = pdf.getDocumentInfo()
        app.logger.debug(pdf_info)
        pdf_title = str(pdf_info.title)
    except:
        app.logger.error("[ERROR] During extracting data from pdf (%s)" % pdf_filename)

    return pdf_title

def move_file_to_processed_dir(filename_in_queue):
    src = PDFS_DIR_IN_QUEUE + str(filename_in_queue.decode(CODING))
    dest = PDFS_DIR_PROCESSED + str(filename_in_queue.decode(CODING))
    try:
        os.rename(src, dest)
        app.logger.debug("Moved file %s" % filename_in_queue)
    except FileNotFoundError as e:
        app.logger.error(e)

def consume_messages():
    channel = open_channel()
    channel.basic_qos(prefetch_count = NUM_OF_TASK_PER_CONSUMER)
    channel.basic_consume(queue = QUEUE_NAME, on_message_callback = callback_mq)
    channel.start_consuming()

consumer__queue_thread = Thread(target = consume_messages)
consumer__queue_thread.start()
```

### Zadania do wykonania w ramach zajęć

1. Zbudować przykładowy projekt z plików źródłowych.
2. Sprawdzić za pomocą `curl` działanie `end-point-u` zwracającego `Hello, World!`.
3. Dorobić walidację sprawdzającą przesłany plik (czy w ogóle jest załączony, i czy jest załączony pod właściwą nazwą).
4. Przenieść konsumenta wiadomości do osobnego kontenera.

